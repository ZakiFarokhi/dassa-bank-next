import Benefit from "@/components/home/Benefit";
import Product from "@/components/home/Product";
import Trusted from "@/components/home/Trusted";
import Slider from "@/components/home/slider";
import News from "@/components/home/News";
export default function Home() {
  return (
    <>
      <div className="w-screen justify-center items-center">
        <Slider />
      </div>
      <div className="container mx-auto py-16">
        <Product />
      </div>
      <div className="w-screen bg-[#035DA8]">
        <div className="container mx-auto py-16">
          <Benefit />
        </div>
      </div>
      <div className="w-screen bg-white">
        <div className="container mx-auto py-16 ">
          <Trusted />
        </div>
      </div>
      <div className="w-screen bg-white">
      <div className="container mx-auto py-16 ">
        <News />
      </div>
      </div>
      {/* <main className="flex min-h-screen flex-col justify-between">
        <div className="px-4 lg:px-8 xl:px-16 lg:py-8 xl:py-16">
          <Product />
        </div>
        <div className="bg-gradient-to-r from-[#112a4f]  to-[#19d5e3] via-[#166282]">
          <Benefit />
        </div>
      </main> */}
    </>
  );
}
