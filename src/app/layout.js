import { Inter } from "next/font/google";
import "./globals.css";
import { AntdRegistry } from "@ant-design/nextjs-registry";
import { ConfigProvider, Typography } from "antd";
import Header from "../components/Header";
import Footer from "../components/Footer";
import ScrollToTop from "../components/ScrollToTop";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Bank Dassa",
  description: "Bank Dassa",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className=" bg-[#035DA8]">
        <ConfigProvider
          theme={{
            token: {
              colorPrimary: "#8DC63F",
              borderRadius: 2,
              colorBgContainer: '#FFFFFF',
            },
            components: {
              Menu: {
                itemColor:'#FFFFFF',
                horizontalItemHoverBg: "rgba(255, 255, 255, 0.2)",
                horizontalItemHoverColor: "rgb(0, 0, 0)",
                horizontalItemSelectedBg: "rgba(255, 255, 255, 0.2)",
                horizontalItemSelectedColor: "rgb(0, 0, 0)",
                itemActiveBg: "rgba(255, 255, 255, 0.2)",
                itemBg: "rgba(255, 255, 255, 0)",
                horizontalItemBorderRadius: 8,  
                lineWidth: 0,
                lineHeight:0,
                
              },
              Tabs: {
                colorText:'#FFFFFF'
              }
            },
          }}
        >
          <Header />
          <AntdRegistry>
            {children}
          </AntdRegistry>
          
          <div className="mt-20">
            <Footer />
          </div>
          <ScrollToTop />
        </ConfigProvider>
      </body>
    </html>
  );
}
