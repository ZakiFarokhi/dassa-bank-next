import Breadcrumb from "../../../components//Common/Breadcrumb";
import CareerDetail from "../../../components/Career/Detail";
import Hero from "../../../components/Career/Hero";

const CareerPageDetail = ({ params }) => {
  return (
    <>
      <Hero />
      <div className="w-screen justify-center items-center">
        <Breadcrumb
          pageName="Karir"
          description="Relokasi ke Graha Anabatic dan rebranding menjadi Bank Dassa"
        />
        <div className="container mx-auto py-16">
          <CareerDetail id={params.id} />{" "}
        </div>
      </div>
    </>
  );
};
export default CareerPageDetail;
