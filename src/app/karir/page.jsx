import Career from "../../components/Career";
import FormFilter from "../../components/Career/Form";
import Breadcrumb from "../../components/Common/Breadcrumb";
import Hero from "../../components/Career/Hero";

import { Metadata } from "next";

export const metadata = {
  title: "Contact Page | Free Next.js Template for Startup and SaaS",
  description: "This is Contact Page for Startup Nextjs Template",
  // other metadata
};

const CareerPage = async () => {
  // console.log(career)
  return (
    <>
      <Hero />

      <div className="container mx-auto py-16">
        <Breadcrumb
          pageName="Karir"
          description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In varius eros eget sapien consectetur ultrices. Ut quis dapibus libero."
        />
        <Career career={[]} />
      </div>
    </>
  );
};

export default CareerPage;
