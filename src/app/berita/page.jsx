import Breadcrumb from "@/components/Common/Breadcrumb";
import News from "../../components/News";
import Hero from "@/components/News/Hero";
import AllNews from "@/components/AllNews";

const NewsPage = async () => {
  return (
    <>
      <Hero />

      <div className="container mx-auto py-8">
        <Breadcrumb pageName="Berita" description="" />
        {/* <News /> */}
        <AllNews />
      </div>
    </>
  );
};
export default NewsPage;
