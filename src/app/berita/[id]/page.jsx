import Breadcrumb from "@/components/Common/Breadcrumb";
import News from "@/components/News";
import NewsDetail from "@/components/News/Detail";
import Hero from "@/components/News/Hero";

const NewsPageDetail = ({ params }) => {
  return (
    <>
      <Hero />
      <div className="w-screen justify-center items-center">
        <div className="container mx-auto py-16">
          <Breadcrumb
            pageName="Berita"
            description="Relokasi ke Graha Anabatic dan rebranding menjadi Bank Dassa"
          />
          <NewsDetail id={params.id} />
        </div>
      </div>
    </>
  );
};
export default NewsPageDetail;
