"use client";
import AboutUs from "@/components/AboutUs";
import { Suspense } from "react";

const AboutPage = () => {
  return (
    <>
      <div className="w-screen">

        <Suspense>
          <AboutUs />
        </Suspense>
      </div>
    </>
  );
};
export default AboutPage;
