import Breadcrumb from "@/components/Common/Breadcrumb";
import Contact from "@/components/Contact";
import Hero from "@/components/Contact/hero";

import { Metadata } from "next";

export const metadata = {
  title: "Contact Page | Free Next.js Template for Startup and SaaS",
  description: "This is Contact Page for Startup Nextjs Template",
  // other metadata
};

const ContactPage = () => {
  return (
    <>
      <Hero />
        <div className="container mx-auto">
          <Breadcrumb
            pageName="Kontak"
            description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In varius eros eget sapien consectetur ultrices. Ut quis dapibus libero."
          />
          <Contact />
        </div>
    </>
  );
};

export default ContactPage;
