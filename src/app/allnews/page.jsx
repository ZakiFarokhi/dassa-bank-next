import Breadcrumb from "@/components/Common/Breadcrumb"
import Hero from "@/components/News/Hero"
import AllNews from '../../components/AllNews'
const NewsPage = async () => {
  return(
    <>
      <Hero />
      <div className="w-screen justify-center items-center">
        <div className="container mx-auto">
        </div>
        <div className="container mx-auto py-16">
        <Breadcrumb pageName="Daftar Berita" description="" />

          <AllNews />
       
        </div>
      </div>
    </>
  )
}
export default NewsPage