import Breadcrumb from "@/components/Common/Breadcrumb"
import Product from "@/components/Product"

const ProductPage = async () => {
  return(
    <>
      <Product />
    </>
  )
}
export default ProductPage