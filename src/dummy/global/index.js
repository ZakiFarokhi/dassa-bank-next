
const GlobalState = {
  company: "Dassa Bank",
  phone: "(021)-22220099",
  fax: "(021)-22220099",
  building: "Northpoint Commercial Unit NB 08 BSD City",
  email: "hello@bankdassa.com",
  address: "Jl BSD Boulevard Utara, Lengkong Kulon, Pagedangan, Tangerang Regency, Banten 15331",
  instagram: "https://www.instagram.com",
  facebook: "http://www.facebook.com",
  twitter: "http://twitter.com",
  linkedin: "http://www.linkedin.com",
  logo: "/images/logo/Logo.png",
};

export default GlobalState;
