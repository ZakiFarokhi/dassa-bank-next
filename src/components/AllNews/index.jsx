import Image from "next/image";
import { Button, Card, Input } from "antd";
import {
  ArrowRightOutlined,
  ClockCircleOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import SectionTitle from "../Common/SectionTitle";
const CardReport = ({ title, href, description, image }) => {
  return (
    <div className=" px-0 mx-2 rounded-xl shadow-lg justify-self-center bg-white">
      <Image
        src={image}
        width={350}
        height={200}
        alt="Card Image"
        className="rounded-t-xl"
      />
      <div className="px-2 py-4">
        <div className="font-bold text-lg mb-2">{title}</div>
        <div className=" text-lg mb-2">{description}</div>
        <a href={href}>
        <Button
          shape="round"
          size="large"
          type="primary"
          className=" rounded-xl my-2"
        >
          Selengkapnya
        </Button>
        </a>
      </div>
      <div className="px-4 pb-2 align-bottom">
        <p className="text-md text-gray-400">
          <ClockCircleOutlined /> 02 Mar, 2022
        </p>
      </div>
    </div>
  );
};
const arr = [];
const AllNews = () => {
  return (
    <section id="tabtwo" className=" overflow-hidden ">
      <div className="flex my-2">
        <div className="flex-initial w-8/12 align-center">
          <div className="flex overflow-x-scroll no-scrollbar gap-2">
            <button className="text-sm rounded-full border border-dassaGreen text-dassaGreen px-2 py-1 hover:bg-dassaGreen hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
              Press Release
            </button>
            <button className="text-sm rounded-full border border-dassaGreen text-dassaGreen px-2 py-1 hover:bg-dassaGreen hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
              Management
            </button>
            <button className="text-sm rounded-full border border-dassaGreen text-dassaGreen px-2 py-1 hover:bg-dassaGreen hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
              Investasi
            </button>
          </div>
        </div>
        <div className="flex-initial w-4/12">
          <Input
            type="round"
            style={{ backgroundColor: "whitesmoke" }}
            size="large"
            placeholder="Cari Berita"
            prefix={<SearchOutlined />}
          />
        </div>
      </div>

      <div className=" ">
        <SectionTitle title="Daftar Berita" paragraph="" dark={false} />
        <div className="rounded-xl shadow-xl bg-white ">
          {/* <div className="w-full bg-[#0A51A1] text-left p-4 rounded-md">
            <p className="text-2xl text-white font-bold">Laporan Tata Kelola</p>

          </div> */}
          <div className="grid grid-cols-4 gap-4 pb-2 bg-[#035DA8]">
          <CardReport
              title={"Bank Dassa bangun kerjasama dengan Prodigi / Broker Asuransi"}
              href={"/berita/1"}
              description={
                "PT Bank Perekonomian Rakyat Dassa (Bank Dassa) Kerjasama dengan PT Perdana Wahana Sentosa (Prodigi) / Broker Asuransi "
              }
              image="/images/news/images-side-nav-items-8.webp"
            />
            <CardReport
              title={"Kerjasama dengan Openbank+"}
              href={"/berita/2"}
              description={
                "Openbank+ adalah sebuah sistem platform yang menyediakan produk dan layanan bekerjasama dengan bank umum dan penyedia layanan pihak ketiga "
              }
              image="/images/news/images-side-nav-items-9.webp"
            />
            <CardReport
              title={"Kemeriahan Peringatan Kemerdekaan Indonesia ke 78"}
              href={"/berita/3"}
              description={
                "Peringatan Hari Kemerdekaan Republik Indonesia yang ke 78 berlangsung dengan seru dan penuh kekeluargaan"
              }
              image="/images/news/images-side-nav-items-5.webp"
            />
            <CardReport
              title={"Kredit kepemilikan ruang dagang di Pasar Kemiri"}
              href={"/berita/4"}
              description={
                "Pasar Kemiri adalah satu-satunya Pasar yang berada di Kecamatan Kemiri Tangerang. "
              }
              image="/images/news/images-side-nav-items-6.webp"
            />
            <CardReport
              title={"Outing Bank Dassa : Ready to the Next Level"}
              href={"/berita/5"}
              description={
                "Ready to the next level, itulah semangat Bank Dassa yang baru untuk menghadapi tantangan perbankan."
              }
              image="/images/news/images-side-nav-items-7.webp"
            />
            <CardReport
              title={"Bank Dassa gelar Dassa Cup untuk pertama kalinya"}
              href={"/berita/6"}
              description={
                "Bank Dassa untuk pertama kalinya menggelar turnamen bulutangkis antar sesama karyawan Bank Dassa."
              }
              image="/images/news/images-side-nav-items-10.webp"
            />
            <CardReport
              title={"Relokasi ke Graha Anabatic"}
              href={"/berita/6"}
              description={
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet purus eget lorem porta "
              }
              image="/images/news/images-side-nav-items-3.webp"
            />
            <CardReport
              title={"Relokasi ke Graha Anabatic"}
              href={"/"}
              description={
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sit amet purus eget lorem porta "
              }
              image="/images/news/images-side-nav-items-4.webp"
            />
            
            
            
          </div>
        </div>
      </div>
    </section>
  );
};

export default AllNews;
