import { Image } from "antd";

const Hero = ({ title }) => {
  return (
    <>
        <div className="w-screen bg-center bg-cover bg-hero py-8 ">
          <div className="flex items-center justify-center w-full h-full  py-12">
            <div className="text-center">
              <div className="container  mx-auto">
                <div className="max-w-4xl mx-auto text-center">
                  
                  <h2 className="my-36  text-4xl lg:text-5xl font-bold text-gray-100">
                    { "Contact"}
                  </h2>
                 
                </div>
              </div>
            </div>
          </div>
        </div>

    </>
  );
};

export default Hero;
