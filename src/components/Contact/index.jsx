import NewsLatterBox from "./NewsLatterBox";
import Accordion from "./Accordion";
import { Image } from "antd";

const Contact = () => {
  const items = [
    {
      title: "Apa itu Bank Dassa ?",
      content: "Bank Dassa adalah Bank Perekonomian Rakyat yang memiliki usaha sebagai penyedia jasa layanan keuangan baik penghimpunan dana masyarakat dalam bentuk simpanan maupun penyaluran dana kepada masayarakat dalam bentuk pinjaman.",
    },
    {
      title: "Dimana Kantor Bank Dassa ?",
      content: "Bank Dassa berlokasi di Northpoint Commercial Park BSD No 08, BSD City, Jl. BSD Boulevard Utara, Lengkong Kulon, Pagedangan, Tangerang Regency, Banten 15331",
    },
    {
      title: "Kenapa harus bank Dassa ?",
      content: "Bank Dassa dalam menjalankan operasionalnya berizin dan diawasi oleh Otoritas Jasa Keuangan (OJK) serta sebagai peserta Lembaga Penjamin Simpanan (LPS).",
    },
    {
      title:
        "Apa syarat menjadi Nasabah Bank Dassa ?",
      content: "Warga Negara Indonesia dengan minimum umur 17 tahun (memiliki Kartu identitas) Untuk buka rekening Bank Dassa: <br/>1. e-KTP Asli 2. NPWP (opsional) 3. Mengisiform pembukaan rekening",
    },
    {
      title: "Fitur apa saja yang dimiliki Bank Dassa?",
      content: "Fitur-fitur yang dimiliki Bank Dassa diantaranya : Smart Inquiry : Aplikasi yang dapat digunakan untuk pengecekan saldo baik simpanan maupun pinjaman. 1. Deposito Digital : Nasabah dapat melakukan simpanan dalam bentuk deposito secara digital melalui platform Komunal dan pilih Bank Dassa sebagai tujuan penempatan deposito anda.",
    },
    {
      title:
        "Berapa persen bunga simpanan maksimal di Bank Dassa? ?",
      content: "Maksimal Bunga LPS yang berlaku saat itu bunga yang dapat diberikan Bank Dassa.",
    },

  ];
  return (
    <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <h2 className=" underline decoration-sky-500 mb-3 text-2xl font-bold text-white dark:text-white sm:text-3xl lg:text-2xl xl:text-3xl">
          Kontak
        </h2>
        <div className="-mx-4 flex flex-wrap  py-4 rounded">
          <div className="w-full px-4 lg:w-1/2 xl:w-1/2 flex flex-col justify-center items-center bg-telephone">
            <div className="items-left">
              <div className="flex justify-left items-left">
                <Image
                  src="/images/contact/ic-email.svg"
                  alt="logo"
                  width={50}
                  height={50}
                  className="dark:block"
                />
              </div>
              <div className="flex justify-left items-center h-fit">
                <p className="text-lime-600">Kontak Kami</p>
              </div>
              <div className="flex justify-left items-center h-fit">
                <h1 className="text-white font-bold">Customer Support</h1>
              </div>
              <div className="flex justify-left items-center h-fit">
                <p className="text-white">Email, Telepon atau isi form untuk</p>
              </div>
              <div className="flex justify-left items-center h-fit">
                <p className="text-white">Mengirim pesan kepada tim DASSA</p>
              </div>
              <div className="flex justify-left items-center h-fit">
                <p className="text-white">(021) 22220099</p>
              </div>
              <div className="flex justify-left items-center h-fit">
                <p className="text-white">hello@bankdassa.com</p>
              </div>
            </div>
          </div>
          <div className="w-full px-4 lg:w-1/2 xl:w-1/2">
            <div
              className="mb-12 rounded-sm bg-white px-8 py-11 shadow-three dark:bg-gray-dark sm:p-[55px] lg:mb-5 lg:px-8 xl:p-[55px]"
              data-wow-delay=".15s
             "
            >
              <h2 className="mb-3 text-2xl font-bold text-black dark:text-white sm:text-3xl lg:text-2xl xl:text-3xl">
                GET IN TOUCH
              </h2>
              <p className="mb-12 text-base font-medium text-body-color">
                Kamu bisa bertanya kapan pun
              </p>
              <form>
                <div className="-mx-4 flex flex-wrap">
                  <div className="w-full px-4 md:w-1/2">
                    <div className="mb-8">
                      <label
                        htmlFor="name"
                        className="mb-3 block text-sm font-medium text-dark dark:text-white"
                      >
                        Nama
                      </label>
                      <input
                        type="text"
                        placeholder="Enter your name"
                        className="border-stroke w-full rounded-sm border bg-[#f8f8f8] px-6 py-3 text-base text-body-color outline-none focus:border-primary dark:border-transparent dark:bg-[#2C303B] dark:text-body-color-dark dark:shadow-two dark:focus:border-primary dark:focus:shadow-none"
                      />
                    </div>
                  </div>
                  <div className="w-full px-4 md:w-1/2">
                    <div className="mb-8">
                      <label
                        htmlFor="email"
                        className="mb-3 block text-sm font-medium text-dark dark:text-white"
                      >
                        Email
                      </label>
                      <input
                        type="email"
                        placeholder="Enter your email"
                        className="border-stroke w-full rounded-sm border bg-[#f8f8f8] px-6 py-3 text-base text-body-color outline-none focus:border-primary dark:border-transparent dark:bg-[#2C303B] dark:text-body-color-dark dark:shadow-two dark:focus:border-primary dark:focus:shadow-none"
                      />
                    </div>
                  </div>
                  <div className="w-full px-4  ">
                    <label
                      htmlFor="email"
                      className="mb-3 block text-sm font-medium text-dark dark:text-white"
                    >
                      Nomor Telepon
                    </label>
                  </div>

                  <div className="w-full flex">
                    <div className="w-full px-4 md:w-1/4">
                      <div className="mb-8">
                        <input
                          type="email"
                          placeholder="+62"
                          className="border-stroke w-full rounded-sm border bg-[#f8f8f8] px-6 py-3 text-base text-body-color outline-none focus:border-primary dark:border-transparent dark:bg-[#2C303B] dark:text-body-color-dark dark:shadow-two dark:focus:border-primary dark:focus:shadow-none"
                        />
                      </div>
                    </div>
                    <div className="w-full px-4 md:w-3/4">
                      <div className="mb-8">
                        <input
                          type="email"
                          placeholder="Phone No."
                          className="border-stroke w-full rounded-sm border bg-[#f8f8f8] px-6 py-3 text-base text-body-color outline-none focus:border-primary dark:border-transparent dark:bg-[#2C303B] dark:text-body-color-dark dark:shadow-two dark:focus:border-primary dark:focus:shadow-none"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="w-full px-4">
                    <div className="mb-8">
                      <label
                        htmlFor="message"
                        className="mb-3 block text-sm font-medium text-dark dark:text-white"
                      >
                        Pesan
                      </label>
                      <textarea
                        name="message"
                        rows={5}
                        placeholder="Masukkan Pesan"
                        className="border-stroke w-full resize-none rounded-sm border bg-[#f8f8f8] px-6 py-3 text-base text-body-color outline-none focus:border-primary dark:border-transparent dark:bg-[#2C303B] dark:text-body-color-dark dark:shadow-two dark:focus:border-primary dark:focus:shadow-none"
                      ></textarea>
                    </div>
                  </div>
                  <div className="w-full px-4">
                    <button className="rounded-sm bg-primary px-9 py-4 text-base font-medium text-white shadow-submit duration-300 hover:bg-primary/90 dark:shadow-submit-dark">
                      Kirim
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="flex flex-wrap ">
          <div className="w-ful lg:w-1/2 xl:w-1/2">
            <div
              className="mb-12 rounded-sm  py-11 shadow-three dark:bg-gray-dark "
              data-wow-delay=".15s
             "
            >
              <Image
                className=""
                alt="benefit dassa bank"
                src="/images/contact/location-map.png"
              />
            </div>
          </div>
          <div className="w-full px-4 lg:w-1/2 xl:w-1/2 flex flex-col justify-center items-left">
            <div className="items-left">
              <div className="ml-16 flex justify-left items-left">
                <Image
                  src="/images/contact/ic-building.svg"
                  alt="logo"
                  width={70}
                  height={70}
                  className="dark:block"
                />
              </div>
              <div className="ml-16 flex justify-left items-center h-fit">
                <p className="text-lime-600 text-xl font-bold">Lokasi Kami</p>
              </div>
              <div className="ml-16 flex justify-left items-center h-fit">
                <h1 className="text-white text-xl font-bold">Northpoint Commercial Unit NB 08 BSD City</h1>
              </div>
              <div className=" ml-16 flex justify-left items-center h-fit">
                <p className="text-white text-lg">Jl BSD Boulevard Utara,</p>
              </div>
              <div className="ml-16 flex justify-left items-center h-fit">
                <p className="text-white">Lengkong Kulon, Pagedangan, Tangerang Regency, Banten 15331</p>
              </div>
            </div>
          </div>
        </div>
        <div className="  py-4 jus  flex flex-col justify-center items-center">
          <h2 className=" mb-3 text-2xl font-bold text-white dark:text-white sm:text-3xl lg:text-2xl xl:text-3xl">
            Frequently Asked Questions
          </h2>
          <div className="w-full">
            <Accordion items={items} />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
