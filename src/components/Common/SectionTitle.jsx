const SectionTitle = ({
  title,
  paragraph,
  width = "570px",
  center,
  mb = "50px",
  dark = true
}) => {
  return (
    <>
      <div
        className={`w-full my-8 ${center ? "mx-auto text-center" : ""}`}
        style={{ maxWidth: width,  }}
      >
        <h2 className={`${dark? "text-white":"text-white"} font-ubuntu mb-2  text-3xl font-bold !leading-tight  dark:text-white sm:text-4xl md:text-[45px]`}>
          {title}
        </h2>
        <p className="text-base text-white font-ubuntu !leading-relaxed text-body-color md:text-lg">
          {paragraph}
        </p>
      </div>
    </>
  );
};

export default SectionTitle;
