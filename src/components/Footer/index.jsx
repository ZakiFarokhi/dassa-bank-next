import GlobalState from "../../dummy/global";
import {
  FacebookOutlined,
  InstagramOutlined,
  LinkedinOutlined,
  TwitterOutlined,
} from "@ant-design/icons";
import { Avatar, Divider } from "antd";
import Image from "next/image";
import Link from "next/link";

const Footer = (props) => {
  const globalState = GlobalState;
  return (
    <>
      <footer className="relative z-10 bg-[#8DC63F]  pt-20 dark:bg-[gray-dark] md:pt-12 lg:pt-12">
        <div className=" container mx-auto">
          <div className=" flex flex-wrap gap-8 justify-between ">
            <div className=" max-w-80">
              <div className=" flex ">
                <Link href="/" className="mb-2 ">
                  <Image
                    // src={baseUrl + image?.url}
                    src={'/images/footer/logo.png'}
                    alt="logo"
                    className="w-full"
                    width={150}
                    height={150}
                  />
                  {/* <p className=" text-xl font-bold self-center text-white font-sauce">
                    Bank Dassa
                  </p> */}
                </Link>
              </div>
              <p className="text-md font-bold text-white font-sauce">
                PT. Bank Perekonomian Rakyat Dassa
              </p>
              {/* <p className="text-sm text-white font-sauce"><span>{data?.building}</span> {data?.address}</p> */}
              <p className="text-sm text-white font-sauce">
                <span>{globalState.company}</span> {globalState.address}
              </p>
            </div>
            <div className="">
              <p className=" text-white font-sauce font-bold text-md">
                Navigation
              </p>
              <p className="my-2 text-white font-sauce text-sm ">
                <Link href="/">Home</Link>
              </p>
              <p className="my-2 text-white font-sauce text-sm">
                <Link href="/">Tentang</Link>
              </p>
              <p className="my-2 text-white font-sauce text-sm">
                <Link href="/">Produk</Link>
              </p>
              <p className="my-2 text-white font-sauce text-sm">
                <Link href="/">Berita</Link>
              </p>
              <p className="my-2 text-white font-sauce text-sm">
                <Link href="/">Karir</Link>
              </p>
              <p className="my-2 text-white font-sauce text-sm">
                <Link href="/">Kontak</Link>
              </p>
            </div>
            <div className=" max-w-96">
              <p className="text-white font-sauce font-bold text-md">
                Disclaimer
              </p>
              <div className="">
                <Link
                  href="/"
                  className="inline-block font-sauce text-white duration-300 hover:text-primary dark:text-body-color-dark dark:hover:text-primary"
                >
                  <Image
                    src="/images/logo/logo-lps.png"
                    alt="logo-lps"
                    width={100}
                    height={50}
                  />
                </Link>
                <p className="my-2 text-white font-sauce text-xs">
                  PT Bank Perekonomian Rakyat Dassa berizin dan diawasi oleh
                  Otoritas Jasa Keuangan (OJK) dan Bank Indonesia, serta
                  merupakan peserta penjaminan LPS
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="mx-8 flex-grow border-t border-gray-400"></div>
        <div className="w-full">
          <div className=" p-4 text-center">
            <p className="text-center text-white font-sauce  dark:text-white">
              &copy; 2023 Bank Dassa. Copyright and rights reserved
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
