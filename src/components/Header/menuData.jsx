
const menuData= [
  {
    id: 1,
    title: "Tentang",
    newTab: false,
    submenu: [
      {
        id: 41,
        title: "Menuju Bank DASSA",
        path: "/tentang?id=1",
        newTab: false,
      },
      {
        id: 42,
        title: "Visi dan Misi",
        path: "/tentang?id=2",
        newTab: false,
      },
      {
        id: 43,
        title: "Filososi",
        path: "/tentang?id=3",
        newTab: false,
      },
      {
        id: 44,
        title: "Nilai Perusahaan",
        path: "/tentang?id=4",
        newTab: false,
      },
      {
        id: 45,
        title: "Pemimpin Kami",
        path: "/tentang?id=6",
        newTab: false,
      },
      {
        id: 46,
        title: "Penghargaan",
        path: "/tentang?id=7",
        newTab: false,
      },
      {
        id: 47,
        title: "Laporan Tata Kelola",
        path: "/tentang?id=5",
        newTab: false,
      },
    ],
  },
  {
    id: 33,
    title: "Produk",
    path: "/produk",
    newTab: false,
  },
  {
    id: 33,
    title: "Berita",
    path: "/berita",
    newTab: false,
  },
  {
    id: 3,
    title: "Karir",
    path: "/karir",
    newTab: false,
  },
  {
    id: 4,
    title: "Kontak",
    path: "/kontak",
    newTab: false,

  },
];
export default menuData;
