import { Divider, Space } from "antd";
import CardProduct from "./CardProduct";

const Product = () => {
  return (
    <>
      <div className="flex flex-wrap  justify-center gap-4 xl:justify-between lg:justify-between">
        <p className="lg:text-left text-3xl font-bold text-dassablue text-white">
          Produk Terbaik <br /> Kami
        </p>
        <CardProduct
          image="/images/home/images-product-1.png"
          title="Tabungan"
          description="Solusi terbaik untuk tabungan Anda dengan rate yang menarik. Nikmati berbagai kemudahan"
          href="/product"
        />
        <CardProduct
          image="/images/home/images-product-2.png"
          title="Kredit"
          description="Dapatkan kredit dengan suku bunga yang kompetitif dan proses cepat"
          href="/product"
        />
        <CardProduct
          image="/images/home/images-product-3.png"
          title="Deposito"
          description="Simpanan berjangka yang aman dengan tingkat suku bunga yang menarik dan berbagai keuntungan"
          href="/product"
        />
      </div>
     
    </>
  );
};

export default Product;
