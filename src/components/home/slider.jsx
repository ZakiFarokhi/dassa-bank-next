import {
  ArrowLeftOutlined,
  LeftCircleOutlined,
  RightCircleOutlined,
} from "@ant-design/icons";
import { Carousel } from "antd";
import Image from "next/image";

const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

// from https://react-slick.neostack.com/docs/example/custom-arrows
const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    />
  );
};

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    />
  );
};

const settings = {
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />,
};

export default async function Slider() {
  return (
    <Carousel
      autoplay
      arrows
      //   nextArrow={<RightCircleOutlined />}
      //   prevArrow={<LeftCircleOutlined />}
    >
      <>
        <div key={1} className="relative h-[600px] w-screen lg:h-screen">
          <Image
            className="h-full w-full  object-cover"
            alt="Mountains"
            src={"/images/home/banner-image-1.png"}
            layout="fill"
          />
          <div className="justify-left absolute inset-0 flex items-center  font-bold text-white lg:text-xl">
            <div className="container mx-auto">
              <div className=" max-w-[600px] text-left">
                <h1 className="mb-5 text-3xl font-bold leading-tight text-white dark:text-white sm:text-4xl sm:leading-tight md:text-5xl md:leading-tight">
                  DASSA
                </h1>
                <p className="mb-4 text-2xl max-w-screen-sm !leading-relaxed text-white dark:text-white sm:text-2xl md:text-2xl">
                  Terpercaya memberikan solusi terbaik untuk tabungan anda
                </p>
                <svg
                  width="487"
                  height="34"
                  viewBox="0 0 487 34"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M4 30C73.6307 10.3798 266.914 -17.0885 483 30"
                    stroke="#F2F2F2"
                    stroke-width="8"
                    stroke-linecap="round"
                  />
                </svg>

                <a href="/">
                  <div className="items-left justify-left mt-8 flex  flex-col">
                    <button className="justify-right flex w-fit items-center rounded-full bg-dassaGreen py-1 pl-6 pr-2 font-sauce font-semibold text-white ">
                      Ajukan sekarang
                      <span className="ml-2 mr-0">
                        <svg
                          width="30"
                          height="30"
                          viewBox="0 0 40 40"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <rect width="40" height="40" rx="20" fill="#F2F2F2" />
                          <path
                            d="M22.2827 12L28.9998 19L22.2827 26"
                            stroke="#9CBD64"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <line
                            x1="27.7329"
                            y1="19.0317"
                            x2="11.9998"
                            y2="19.0317"
                            stroke="#9CBD64"
                            strokeWidth="2"
                            strokeLinecap="round"
                          />
                        </svg>
                      </span>
                    </button>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div key={2} className="relative h-[600px] w-screen lg:h-screen">
          <Image
            className="h-full w-full  object-cover"
            alt="Mountains"
            src={"/images/home/banner-image-2.png"}
            layout="fill"
          />
          <div className="justify-left absolute inset-0 flex items-center  font-bold text-white lg:text-xl">
            <div className="container mx-auto">
              <div className="float-left  max-w-[600px] text-left">
                <h1 className="mb-5 font-sauce text-3xl font-bold leading-tight text-white dark:text-white sm:text-4xl sm:leading-tight md:text-5xl md:leading-tight">
                  DASSA
                </h1>
                <p className="mb-4 font-sauce text-2xl !leading-relaxed text-white dark:text-white sm:text-2xl md:text-2xl">
                  Terpercaya memberikan <br className="block xl:hidden" />{" "}
                  solusi terbaik untuk
                  <br className="block xl:hidden" />
                  tabungan anda
                </p>
                <svg
                  width="487"
                  height="34"
                  viewBox="0 0 487 34"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M4 30C73.6307 10.3798 266.914 -17.0885 483 30"
                    stroke="#F2F2F2"
                    stroke-width="8"
                    stroke-linecap="round"
                  />
                </svg>

                <a href="/">
                  <div className="items-left justify-left mt-8 flex  flex-col">
                    <button className=" justify-right flex w-fit items-center rounded-full bg-dassaGreen py-1 pl-6 pr-2 font-sauce font-semibold text-white ">
                      Ajukan sekarang
                      <span className="ml-2 mr-0">
                        <svg
                          width="30"
                          height="30"
                          viewBox="0 0 40 40"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <rect width="40" height="40" rx="20" fill="#F2F2F2" />
                          <path
                            d="M22.2827 12L28.9998 19L22.2827 26"
                            stroke="#9CBD64"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                          <line
                            x1="27.7329"
                            y1="19.0317"
                            x2="11.9998"
                            y2="19.0317"
                            stroke="#9CBD64"
                            strokeWidth="2"
                            strokeLinecap="round"
                          />
                        </svg>
                      </span>
                    </button>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div key={3} className="relative h-[600px] w-screen lg:h-screen">
          <Image
            className="h-full w-full  object-cover"
            alt="Mountains"
            src={"/images/home/banner-image-3.png"}
            layout="fill"
          />
          <div className="justify-left absolute inset-0 flex items-center  font-bold text-white lg:text-xl">
            <div className="container">
              
            </div>
          </div>
        </div>

        <div key={2} className="relative h-[600px] w-screen lg:h-screen">
          <Image
            className="h-full w-full  object-cover"
            alt="Mountains"
            src={"/images/home/banner-image-4.png"}
            layout="fill"
          />
          <div className="justify-left absolute inset-0 flex items-center  font-bold text-white lg:text-xl">
            <div className="container">
              
            </div>
          </div>
        </div>
      </>
    </Carousel>
  );
}
