import React from 'react';
import { ArrowRightOutlined, EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import { Avatar, Button, Card, Image, Space } from 'antd';
import Link from 'next/link';

const { Meta } = Card;

const CardProduct = ({ title, description, href, image }) => (
  <>

    <div className='w-[300px] h-[430px] rounded-xl shadow-lg bg-white py-4 px-4 my-2'>
      <Image
        alt="example"
        style={{ borderRadius: '10px' }}
        className='rounded-xl'
        src={image}
      />
      <div key="content-card" className='py-4'>
        <p className='text-left font-bold text-2xl text-dassablue'>{title}</p>
        <p className='text-left font-bold text-md text-dassablue'>{description}</p>
      </div>
      <div key="content-action" className='h-max  w-full text-left'>
        <Link href={href}><Button size='large' shape='round' type="primary" iconPosition='end' icon={<ArrowRightOutlined />} ><span className='font-semibold'>Selengkapnya</span></Button></Link>
      </div>
    </div>
  </>
);

export default CardProduct;