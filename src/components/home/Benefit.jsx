import {
  CheckCircleFilled,
  CheckCircleOutlined,
  CheckCircleTwoTone,
} from "@ant-design/icons";
import { Col, Divider, Image, Row, Space } from "antd";

const Benefit = () => {
  return (
    <>
      <div className="flex flex-wrap justify-between">
        <div className="">
          <Image
            className=""
            alt="benefit dassa bank"
           
            src="/images/home/benefit-image.png"
          />
        </div>
        <div className="self-center ">
          <p className="text-3xl text-white font-semibold">
            Keuntungan yang akan <span className="underline">anda dapat</span>
          </p>
          <ul className="mt-8">
            <li className="my-4 text-white font-sauce text-lg font-semibold">
              <CheckCircleFilled
                style={{ fontSize: "20px", color: "#52c41a" }}
              />{" "}
              Proses Online
            </li>
            <li className="my-4 text-white font-sauce text-lg font-semibold">
              <CheckCircleFilled
                style={{ fontSize: "20px", color: "#52c41a" }}
              />{" "}
              Cepat
            </li>
            <li className="my-4 text-white font-sauce text-lg font-semibold">
              <CheckCircleFilled
                style={{ fontSize: "20px", color: "#52c41a" }}
              />{" "}
              Aman dan Terpercaya
            </li>
            <li className="my-4 text-white font-sauce text-lg font-semibold">
              <CheckCircleFilled
                style={{ fontSize: "20px", color: "#52c41a" }}
              />{" "}
              Di bantu oleh tim profesional
            </li>
          </ul>
        </div>
      </div>
      
    </>
  );
};

export default Benefit;
