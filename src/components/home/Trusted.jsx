import { Image, Space } from "antd";
import Link from "next/link";

const Trusted = () => {
  return (
    <>
    
      <p className="text-center text-ubuntu my-8 xl:text-3xl sm:text-xl font-bold text-dassablue">
        Telah Dipercaya
      </p>
      <div className="flex flex-wrap justify-center gap-4 xl:justify-between lg:justify-between">
      <Link className="" href={"https://www.pendanaan.com"} target={"_blank"}>
        <Image
          preview={false}
          src="/images/logo/logo-pendanaan.png"
          width={200}
          alt="logo-pendanaan"
        />
      </Link>
      <Link href={"https://www.prodiginow.com"} target={"_blank"}>
        <Image
          preview={false}
          src="/images/logo/logo-prodigi.png"
          width={200}
          alt="logo-prodigi"
        />
      </Link>
      <Link href={"https://www.komunalgroup.com"} target={"_blank"}>
        <Image
          preview={false}
          src="/images/logo/logo-komunal.png"
          width={200}
          alt="logo-komunal"
        />
      </Link>
      <Link href={"https://lineicons.com"} target={"_blank"}>
        <Image
          preview={false}
          src="/images/logo/logo-openbank.png"
          width={200}
          alt="logo-pendanaan"
        />
      </Link>
      </div>
    </>
  );
};

export default Trusted;
