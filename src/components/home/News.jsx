import {
  ArrowRightOutlined,
  CheckCircleFilled,
  CheckCircleOutlined,
  CheckCircleTwoTone,
} from "@ant-design/icons";
import { Col, Divider, Row, Space } from "antd";
import Image from "next/image";

const News = () => {
  return (
    <>
      <div className="flex flex-nowrap justify-between my-2">
        <p className="text-ubuntu  text-3xl font-bold text-white">Berita</p>
        <p className="text-ubuntu cursor-pointer text-3xl font-bold text-white">
          Selengkapnya <ArrowRightOutlined />
        </p>
      </div>

      <div className="flex flex-col ">
        <div className="flex overflow-x-scroll no-scrollbar py-4 rounded-xl ">
          <div className="flex flex-nowrap m-2 ">
            <div className="inline-block pr-3">
              <div className="align-center flex w-[500px] h-fit overflow-hidden rounded-lg shadow-md bg-patternCard hover:shadow-xl transition-shadow duration-300 ease-in-out">
                <Image
                  src="/images/home/news-card-images-1.png"
                  alt="Meeting"
                  width="0"
                  height="0"
                  sizes="100vw"
                  className="h-[200px] w-[200px]"
                />
                <div className="p-4 content-center">
                  <h2 className="text-lg font-bold text-dassaBlue">Hari batik nasional</h2>
                  <p className="text-dassaBlue">
                    Karyawan PT BPR Dassa Turut Berpartisipasi pada Hari Batik
                    Nasional
                  </p>
                </div>
              </div>
            </div>
            <div className="inline-block px-3">
            <div className="align-center flex w-[500px] h-fit overflow-hidden rounded-lg shadow-md bg-patternCard hover:shadow-xl transition-shadow duration-300 ease-in-out">
                <Image
                  src="/images/home/news-card-images-1.png"
                  alt="Meeting"
                  width="0"
                  height="0"
                  sizes="100vw"
                  className="h-[200px] w-[200px]"
                />
                <div className="p-4 content-center">
                  <h2 className="text-lg font-bold text-dassaBlue">Hari batik nasional</h2>
                  <p className="text-dassaBlue">
                    Karyawan PT BPR Dassa Turut Berpartisipasi pada Hari Batik
                    Nasional
                  </p>
                </div>
              </div>
            </div>
            <div className="inline-block px-3">
            <div className="align-center flex w-[500px] h-fit overflow-hidden rounded-lg shadow-md bg-patternCard hover:shadow-xl transition-shadow duration-300 ease-in-out">
                <Image
                  src="/images/home/news-card-images-1.png"
                  alt="Meeting"
                  width="0"
                  height="0"
                  sizes="100vw"
                  className="h-[200px] w-[200px]"
                />
                <div className="p-4 content-center">
                  <h2 className="text-lg font-bold text-dassaBlue">Hari batik nasional</h2>
                  <p className="text-dassaBlue">
                    Karyawan PT BPR Dassa Turut Berpartisipasi pada Hari Batik
                    Nasional
                  </p>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </>
  );
};

export default News;
