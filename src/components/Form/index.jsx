'use client';
import React, { useState } from 'react';
import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Mentions,
  Modal,
  Select,
  TreeSelect,
} from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const FormBank= () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [name, setName] = useState('')
  const [telephone, setTelephone] = useState('')
  const [email, setEmail] = useState('')
  const [province, setProvince] = useState('')
  const [city, setCity] = useState('')
  const [nominal, setNominal] = useState('')


  const showModal = () => {
    setIsModalOpen(true);
  };

  return (
    <div>
      <button onClick={showModal} className="font-semibold text-lg bg-dassaGreen  hover:bg-blue-700 text-white py-3 px-8 rounded-full">
        Ajukan Sekarang &nbsp;
        <ArrowRightOutlined style={{ fontSize: 20 }} />
      </button>
      <Modal onCancel={() => setIsModalOpen(false)} width={600} style={{ textAlign: "center" }} title="Form Pengajuan" open={isModalOpen} footer={''} >
        <div className='text-center'>
          <Form {...formItemLayout} size='large' style={{ textAlign: "center" }} layout='vertical' variant="filled" className='w-full'>
            <Form.Item label="Nama" name="Input" rules={[{ required: true, message: 'Please input!' }]}>
              <Input value={name} size='large' />
            </Form.Item>

            <Form.Item
              label="Nomor Telepon"
              name="InputNumber"
              rules={[{ required: true, message: 'Please input!' }]}
            >
              <InputNumber value={telephone} size='large' style={{ width: '100%' }} />
            </Form.Item>

            <Form.Item label="Email" name="Input" rules={[{ required: true, message: 'Please input!' }]}>
              <Input size='large' value={email} />
            </Form.Item>


            <Form.Item label="Provinsi" name="Input" rules={[{ required: true, message: 'Please input!' }]}>
              <Input size='large' value={province} />
            </Form.Item>

            <Form.Item label="Kota" name="Input" rules={[{ required: true, message: 'Please input!' }]}>
              <Input size='large' value={city} />
            </Form.Item>

            <Form.Item label="Rencana Nominal" name="Input" rules={[{ required: true, message: 'Please input!' }]}>
              <Input size='large' value={nominal} />
            </Form.Item>
            <Form.Item >
              <button className="font-semibold text-lg bg-dassaGreen hover:bg-blue-700  text-white py-3 px-8 rounded-full">
                Kirim &nbsp;
                <ArrowRightOutlined style={{ fontSize: 20 }} />
              </button>
            </Form.Item>
          </Form>
        </div>

      </Modal>
    </div >
  )
};

export default FormBank;