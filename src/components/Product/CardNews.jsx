import { ArrowRightOutlined, ForwardOutlined, ShareAltOutlined } from "@ant-design/icons";
import { Card } from "antd";
import Image from "next/image";
import Link from "next/link";
const { Meta } = Card

export default function CardNews({ images, title, description, id }) {
  console.log("ini")
  console.log(images.url)
  return (
    <div className="w-full px-4 py-4  rounded overflow-hidden shadow-lg justify-self-center">
      <Image src={ images?.url} width={850} height={500} alt="Card Image" />
      <div className="px-4 py-4">
        <div className="font-bold text-2xl mb-2">{title}</div>
        <p className="text-gray-700 text-base">{description}</p>
      </div>
      <div className="px-4 py-4 flex justify-between">
        <Link href={`/news/${id}`}>
          <button className="font-semibold text-lg bg-dassaGreen  hover:bg-blue-700  text-white py-3 px-8 rounded-full">
            Selengkapnya &nbsp;
            <ArrowRightOutlined style={{ fontSize: 20 }} />
          </button>
        </Link>

        <ShareAltOutlined size={64} style={{ color: "GrayText" }} />
      </div>
    </div>
  )
}