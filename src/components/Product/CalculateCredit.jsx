import React, { useState } from 'react';
import { Button, Form, Input, Select, Modal, Table } from 'antd';

const { Column } = Table;

const CalculateCredit = () => {
  const [result, setResult] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [nominal, setNominal] = useState('');
  const [tenor, setTenor] = useState('1');
  const [bunga] = useState('10');
  const [totalAng, setTotalAng] = useState('')

  const calculate = () => {
    setLoading(true);
    const nominalFix = parseFloat(nominal.replace(/Rp\. |\./g, '').replace(',', '.'));
    const bungaFix = parseFloat(bunga);
    const tenorFix = parseInt(tenor) * 12;
    
    const monthlyPayment = calculateAnnuityPayment(nominalFix, bungaFix, tenorFix);
    
    const rows = [];
    let remainingPrincipal = nominalFix;
    for (let i = 1; i <= tenorFix; i++) {
      const interest = remainingPrincipal * bungaFix / 100 / 12;
      const principal = monthlyPayment - interest;
      remainingPrincipal -= principal;
      setTotalAng(formatCurrency(principal + interest))
      rows.push({
        month: i,
        principal: formatCurrency(principal),
        interest: formatCurrency(interest),
        total:formatCurrency(principal + interest),
        remainingPrincipal: formatCurrency(Math.max(remainingPrincipal, 0)),
      });
    }

    setResult(rows);
    setLoading(false);
    setOpenModal(true);
  };

  function calculateAnnuityPayment(P, r, n) {
    r = r / 100 / 12;

    const A = (P * r * Math.pow(1 + r, n)) / (Math.pow(1 + r, n) - 1);

    return A;
  }

  const formatCurrency = (value) => {
    return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(value);
  };
  const nominalChange = (e) => {
    const inputNominal = e.target.value;
    const formattedNominal = inputNominal.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    setNominal(formattedNominal);
  };

  const tenorChange = (value) => {
    setTenor(value);
  };

  return (
    <div className="p-4 rounded-xl border-2 shadow-xl">
      <div className="rounded-xl text-center border-2 text-xl font-bold">Kalkulator Kredit</div>
      <div>
        <Form layout="vertical" style={{ maxWidth: 1000 }}>
          <Form.Item label="Plafon">
            <Input placeholder='10.000.000' prefix="Rp. " onChange={nominalChange} value={nominal} size="large" style={{ backgroundColor: 'whitesmoke' }} />
          </Form.Item>
          <Form.Item label="Jangka Waktu (Tenor)">
            <Select value={tenor} onChange={tenorChange}>
              <Select.Option value="1">1 Tahun</Select.Option>
              <Select.Option value="2">2 Tahun</Select.Option>
              <Select.Option value="3">3 Tahun</Select.Option>
              <Select.Option value="4">4 Tahun</Select.Option>
              <Select.Option value="5">5 Tahun</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="Bunga">
            <Select disabled value={bunga}>
              <Select.Option value="10">10 %</Select.Option>
            </Select>
          </Form.Item>
        </Form>
        <Button
          onClick={() => calculate()}
          shape="round"
          size="large"
          style={{ backgroundColor: '#0A51A1', color: 'white', marginTop: '20px' }}
          loading={loading}
        >
          Kalkulasi
        </Button>
      </div>
      <Modal
        title="Kalkulasi Kredit"
        width={'1000px'}
        visible={openModal}
        onCancel={() => setOpenModal(false)}
        footer={null}
      >
        <p className='font-bold text-xl font-ubuntu text-dassaBlue'> Total Angsuran : {totalAng}</p>
        <Table dataSource={result} pagination={false}>
          <Column title="Bulan" dataIndex="month" key="month" />
          <Column title="Angsuran Pokok" dataIndex="principal" key="principal" />
          <Column title="Angsuran Bunga" dataIndex="interest" key="interest" />
          <Column title="Total Angsuran" dataIndex="total" key="total"/>
          <Column title="Sisa Pokok" dataIndex="remainingPrincipal" key="remainingPrincipal" />
        </Table>
      </Modal>
    </div>
  );
};

export default CalculateCredit;
