import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"
import CalculateDeposito from "./CalculateDeposito"
import FormBank from "../Form"

const DepositoOne = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <SectionTitle title="Deposito" paragraph="" dark={false} />
        <div className="flex ">
          <div className="flex-initial mx-2 w-2/3">
            <p className="text-xl">
            Simpanan berjangka yang aman dengan tingkat bunga yang menarik, jangka waktu yang fleksibel, dan berbagai keuntungan lainnya
            </p>
            <p className="mt-8 text-2xl font-bold">
              Manfaat
            </p>
            <ul className="pl-10 list-decimal mx-2 my-4  text-xl">
              <li>Jangka waktu  : 1, 3, 6, dan 12 bulan</li>
              <li>Dapat diperpanjang secara otomatis (ARO - Automatic Roll Over) atau tidak otomatis (Non-ARO)</li>
              <li>Bunga dapat ditransfer ke rekening tabungan</li>
              <li>Aman, dijamin oleh Lembaga Penjamin Simpanan (LPS) </li>
            </ul>
            <p className="mt-8 text-2xl font-bold">
              Syarat & Ketentuan
            </p>
            <ul className="pl-10 list-decimal mx-2 my-4  text-xl">
              <li>Membawa bukti identitas diri yang masih berlaku, yaitu :

                <ul className="list-none">
                  <li className="indent-8">Perorangan : KTP (WNI) KITAS/Passport (WNA).</li>
                  <li className="indent-8">Badan Usaha : SIUP, TDP, NPWP.</li>
                </ul>
              </li>
              <li>Mengisi dan menandatangani formulir aplikasi data pengajuan deposito.</li>
              <li>Mengisi dan menandatangani formulir pembukaan rekening apabila bunga ditransfer ke rekening tabungan.</li>
            </ul>
            <FormBank />

          </div>
          <div className="flex-initial mx-2 w-1/3">
            <CalculateDeposito />
          </div>
        </div>
      </div>
    </section>
  )
}

export default DepositoOne