import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"
import FormBank from "../Form"

const StudentOne = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className=" ">
        <SectionTitle title="Tabungan Pelajar  " paragraph="" dark={false} />
        <p className="text-xl">
        Solusi terbaik untuk tabungan Anda dengan rate yang menarik. Tabungan pelajar ditujukan khusus kepada segmen anak dengan fasilitas dan fitur yang menarik bagi anak. Tabungan ini hadir sebagai bentuk perhatian Bank Dassa yang peduli terhadap perkembangan generasi muda Indonesia, untuk membentuk karakter budaya menabung, mengelola keuangan, dan mempersiapkan masa depan di kalangan pelajar. Nikmati berbagai kemudahan untuk pembukaan rekening, setor tunai, maupun tarik tunai.        </p>
        <p className="mt-8 text-2xl font-bold">
          Manfaat
        </p>
        <ul className="pl-10 list-decimal mx-2 my-4 w-2/3 text-xl">
          <li>Suku Bunga Menarik</li>
          <li>Dapat diperpanjang secara otomatis (ARO - Automatic Roll Over) atau tidak otomatis (Non-ARO)</li>
          <li>Bunga dapat ditransfer ke rekening tabungan</li>
          <li>Aman, dijamin oleh Lembaga Penjamin Simpanan (LPS) </li>
        </ul>
        <p className="mt-8 text-2xl font-bold">
          Syarat & Ketentuan
        </p>
        <ul className="pl-10 list-decimal mx-2 my-4 w-2/3 text-xl">
          <li>Membawa bukti identitas diri yang masih berlaku, yaitu :

            <ul className="list-none">
              <li className="indent-8">Perorangan : KTP (WNI) KITAS/Passport (WNA).</li>
              <li className="indent-8">Badan Usaha : SIUP, TDP, NPWP.</li>
            </ul>
          </li>
          <li>Mengisi dan menandatangani formulir aplikasi data pengajuan deposito.</li>
          <li>Mengisi dan menandatangani formulir pembukaan rekening apabila bunga ditransfer ke rekening tabungan.</li>
        </ul>
        <FormBank />
      </div>
    </section>
  )
}

export default StudentOne