'use client'
import { Tabs } from "antd"
import SectionTitle from "../Common/SectionTitle"
import Icon, { RocketOutlined } from "@ant-design/icons"
import { useState } from "react";
import BusinessOne from "./BusinessOne";
import BusinessTwo from "./BusinessTwo";
import DepositoOne from "./DepositoOne";
import DepositoTwo from "./DepositoTwo";

const { TabPane } = Tabs;
const Deposito = () => {
  const [activeKey, setActiveKey] = useState('1')
  const onKeyChange = (key) => setActiveKey(key)
  return (
    <section id="about" className="overflow-visible h-fit py-2 md:py-4 lg:py-8">
      <div className="h-full mb-20">

        <Tabs type="card" defaultActiveKey="1" size="large" activeKey={activeKey} onChange={onKeyChange}>
          <TabPane tab="Deposito" key="1" >
            <DepositoOne />
          </TabPane>
          <TabPane tab="Suku Bunga" key="2"  >
            <DepositoTwo />
          </TabPane>

        </Tabs>

        

      </div>


    </section>

  )
}


export default Deposito