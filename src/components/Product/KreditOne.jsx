import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"
import Image from "next/image"
import CalculateDeposito from "./CalculateDeposito"
import CalculateCredit from "./CalculateCredit"
import FormBank from "../Form"

const KreditOne = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className=" ">
        <SectionTitle title="Kredit Multiguna" paragraph="" dark={false} />
        <div className="flex">
          <div className="flex-initial mx-2 w-2/3">
            <p className="text-xl">
              Produk Kredit Multiguna adalah kredit dengan tujuan penggunaan untuk keperluan apa saja, dengan agunan berupa rumah dan atau ruko di lokasi yang marketable, tidak dalam status atau potensi sengketa.
              Maksimum plafon kredit adalah sebesar <span className="font-bold">70% dari nilai pasar</span> rumah dan atau ruko yang di jaminkan.        </p>
            <p className="mt-8 text-2xl font-bold">
              Manfaat
            </p>
            <ul className="pl-10 list-decimal mx-2 my-4  text-xl">
              <li>Nama Produk  : Kredit Multiguna</li>
              <li>Jumlah Plafon  : Min. Rp. 5.000.000,-</li>
              <li>Jangka Waktu  : s/d maks 5 tahun</li>
              <li>Nilai Pembiayaan  : Maks. 70% dari Nilai Pasar</li>
              <li>Angsuran  : Bulanan</li>
              <li>Segmen Nasabah  : Perorangan & Perusahaan</li>
              <li>Syarat  : Asuransi Jiwa Kredit, Asuransi Kerugian (Agunan)</li>
              <li>Agunan  : Rumah dan atau Ruko berlokasi di Jabodetabek, sesuai syarat terlampir</li>
              <li>Dokumen  : Sesuai syarat dokumen terlampir</li>
            </ul>
            <p className="mt-8 text-2xl font-bold">
              Syarat & Ketentuan
            </p>
            <ul className="pl-10 list-decimal mx-2 my-4 text-xl">
              <li>Copy KTP (suami/istri) :

                <ul className="list-none">
                  <li className="indent-8">Karyawan tetap (usia 21 – 60 tahun s/d jatuh tempo kredit)</li>
                  <li className="indent-8">Wiraswasta (usia 21 – 60 tahun s/d jatuh tempo kredit)</li>
                  <li className="indent-8">Badan Usaha PD/UD, CV (usia ≥ 2 tahun setelah berdiri)</li>

                </ul>
              </li>
              <li>Calon Nasabah disarankan agar mempersiapkan kelengkapan dokumen penunjang guna memudahkan petugas kami saat melakukan penjemputan dokumen dan proses pengajuan kredit multiguna bisa segera dilakukan.</li>
            </ul>
            <div className="text-left">
              <Image
                src="/images/product/credit-image.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ textAlign: 'center', width: '500px', height: '800' }} // optional
              />
            </div>
            <FormBank />

          </div>
          <div className="flex-initial mx-2 w-1/3">
            <CalculateCredit />
          </div>
        </div>

      </div>
    </section>
  )
}

export default KreditOne