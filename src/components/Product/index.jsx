'use client'
import Image from "next/image";
import CardNews from "./CardNews";
import {  Menu, MenuProps, Tabs } from "antd";
import Icon, { AppstoreOutlined, BankOutlined, MailOutlined, SettingOutlined } from "@ant-design/icons";
import TabPane from "antd/es/tabs/TabPane";
import { useState } from "react";
import Business from "./Business";
import Student from "./Student";
import Deposito from "./Deposito";
import Kredit from "./Kredit";
import Hero from "./hero";
import Breadcrumb from "../Common/Breadcrumb";
function getItem(
  label,
  key,
  icon,
  children,
  type,
) {
  return {
    key,
    icon,
    children,
    label,
    type,
  } ;
}

const ICSavings = () => (
  <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M14.7844 8.74375C14.1166 9.21986 13.2994 9.49992 12.4167 9.49992C10.1615 9.49992 8.33337 7.67175 8.33337 5.41659C8.33337 3.16142 10.1615 1.33325 12.4167 1.33325C13.8785 1.33325 15.1609 2.10141 15.8823 3.25608M6.00004 22.4349H9.04538C9.44244 22.4349 9.83707 22.4822 10.222 22.5767L13.4397 23.3586C14.1379 23.5287 14.8653 23.5452 15.5708 23.4082L19.1285 22.7161C20.0683 22.533 20.9329 22.0829 21.6104 21.4238L24.1276 18.9752C24.8464 18.2772 24.8464 17.1444 24.1276 16.4452C23.4804 15.8156 22.4556 15.7447 21.7234 16.2786L18.7897 18.4189C18.3696 18.726 17.8584 18.8914 17.3326 18.8914H14.4998L16.303 18.8913C17.3193 18.8913 18.1425 18.0905 18.1425 17.1018V16.7439C18.1425 15.923 17.5682 15.2072 16.7498 15.0088L13.9667 14.332C13.5138 14.2221 13.05 14.1666 12.5837 14.1666C11.4581 14.1666 9.42058 15.0985 9.42058 15.0985L6.00004 16.5289M22.3334 6.58325C22.3334 8.83841 20.5052 10.6666 18.25 10.6666C15.9949 10.6666 14.1667 8.83841 14.1667 6.58325C14.1667 4.32809 15.9949 2.49992 18.25 2.49992C20.5052 2.49992 22.3334 4.32809 22.3334 6.58325ZM1.33337 16.0333L1.33337 22.7999C1.33337 23.4533 1.33337 23.78 1.46053 24.0296C1.57239 24.2491 1.75086 24.4276 1.97038 24.5394C2.21995 24.6666 2.54665 24.6666 3.20004 24.6666H4.13337C4.78677 24.6666 5.11347 24.6666 5.36303 24.5394C5.58255 24.4276 5.76103 24.2491 5.87288 24.0296C6.00004 23.78 6.00004 23.4533 6.00004 22.7999V16.0333C6.00004 15.3799 6.00004 15.0532 5.87288 14.8036C5.76103 14.5841 5.58255 14.4056 5.36303 14.2937C5.11347 14.1666 4.78677 14.1666 4.13337 14.1666L3.20004 14.1666C2.54665 14.1666 2.21995 14.1666 1.97039 14.2937C1.75086 14.4056 1.57239 14.5841 1.46053 14.8036C1.33337 15.0532 1.33337 15.3799 1.33337 16.0333Z" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
)
const ICBuilding = () => (
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M3.83333 8.5001V17.8334M9.08333 8.5001V17.8334M14.9167 8.5001V17.8334M20.1667 8.5001V17.8334M1.5 19.7001L1.5 20.6334C1.5 21.2868 1.5 21.6135 1.62716 21.8631C1.73901 22.0826 1.91749 22.2611 2.13701 22.3729C2.38657 22.5001 2.71327 22.5001 3.36667 22.5001H20.6333C21.2867 22.5001 21.6134 22.5001 21.863 22.3729C22.0825 22.2611 22.261 22.0826 22.3728 21.8631C22.5 21.6135 22.5 21.2868 22.5 20.6334V19.7001C22.5 19.0467 22.5 18.72 22.3728 18.4704C22.261 18.2509 22.0825 18.0724 21.863 17.9606C21.6134 17.8334 21.2867 17.8334 20.6333 17.8334H3.36667C2.71327 17.8334 2.38657 17.8334 2.13701 17.9606C1.91749 18.0724 1.73901 18.2509 1.62716 18.4704C1.5 18.72 1.5 19.0467 1.5 19.7001ZM11.5951 1.59009L2.96173 3.50861C2.44016 3.62451 2.17937 3.68247 1.98471 3.82271C1.81301 3.94641 1.67818 4.11448 1.59467 4.30893C1.5 4.52938 1.5 4.79653 1.5 5.33082L1.5 6.63344C1.5 7.28683 1.5 7.61353 1.62716 7.86309C1.73901 8.08262 1.91749 8.26109 2.13701 8.37294C2.38657 8.5001 2.71327 8.5001 3.36667 8.5001H20.6333C21.2867 8.5001 21.6134 8.5001 21.863 8.37294C22.0825 8.26109 22.261 8.08262 22.3728 7.86309C22.5 7.61353 22.5 7.28683 22.5 6.63344V5.33083C22.5 4.79653 22.5 4.52938 22.4053 4.30893C22.3218 4.11448 22.187 3.94641 22.0153 3.82271C21.8206 3.68247 21.5598 3.62451 21.0383 3.50861L12.4049 1.59009C12.2538 1.55651 12.1783 1.53972 12.1019 1.53302C12.0341 1.52708 11.9659 1.52708 11.8981 1.53302C11.8217 1.53972 11.7462 1.55651 11.5951 1.59009Z" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
)

const ICCredit = () => (
  <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M24.6667 9.66667H1.33337M11.8334 20.1667L20.9334 20.1667C22.2402 20.1667 22.8936 20.1667 23.3927 19.9123C23.8317 19.6886 24.1887 19.3317 24.4124 18.8926C24.6667 18.3935 24.6667 17.7401 24.6667 16.4333V7.56667C24.6667 6.25988 24.6667 5.60648 24.4124 5.10736C24.1887 4.66831 23.8317 4.31136 23.3927 4.08765C22.8936 3.83333 22.2402 3.83333 20.9334 3.83333H18.8334M11.8334 20.1667L14.1667 22.5M11.8334 20.1667L14.1667 17.8333M7.16671 20.1667H5.06671C3.75992 20.1667 3.10652 20.1667 2.6074 19.9123C2.16835 19.6886 1.8114 19.3317 1.58769 18.8926C1.33337 18.3935 1.33337 17.7401 1.33337 16.4333V7.56667C1.33337 6.25988 1.33337 5.60648 1.58769 5.10736C1.8114 4.66831 2.16835 4.31136 2.6074 4.08765C3.10652 3.83333 3.75992 3.83333 5.06671 3.83333H14.1667M14.1667 3.83333L11.8334 6.16667M14.1667 3.83333L11.8334 1.5" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
  </svg>

)

const items = [
  getItem('Tabungan', 'sub1', <Icon component={ICSavings} />, [
    getItem('Tabungan Bisnis', '1'),
    getItem('Tabungan Pelajar', '2'),
  ]),

  getItem('Deposito', '3', <Icon component={ICBuilding} />),
  getItem('Kredit', '4', <Icon component={ICCredit} />),
];
const urlImages = [
  "/images/product/hero_product_1.png",
  "/images/product/hero_product_2.png",
  "/images/product/hero_product_3.png",
  "/images/product/hero_product_4.png"
]

const Product = () => {
  // console.log(props.blogs)
  const [current, setCurrent] = useState('1');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  const Content = () => {
    if (parseInt(current) === 1) {
      return (
        <Business />
      )
    } else if (parseInt(current) == 2) {
      return (
        <Student />
      )
    } else if (parseInt(current) == 3) {
      return (
        <Deposito />
      )
    } else if (parseInt(current) == 4) {
      return (
        <Kredit />
      )
    }
  }
  return (
    <>
      <Hero url={urlImages[parseInt(current)-1]} />
      <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
        <div className="container mx-auto">
        <Breadcrumb pageName="Produk" description="" />
          <div className="flex flex-row">
            <div className="basis-1/6">
              <Menu
                theme="light"
                onClick={onClick}
                style={{ width: 256,  }}
                defaultOpenKeys={['sub1']}
                selectedKeys={[current]}
                mode="inline"
                items={items}
              />
            </div>
            <div className="basis-5/6">
              <Content />
            </div>
          </div>
        </div>
      </section>
    </>

  )
}

export default Product
