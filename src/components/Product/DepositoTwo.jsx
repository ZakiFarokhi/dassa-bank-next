import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"

const DepositoTwo = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className=" ">
        <SectionTitle title="Suku Bunga " paragraph="" dark={false} />
        <p className="text-red-500 text-xl my-4">
        TELAH DIPERBARUI 1 November 2023.</p>
        <p className="text-xl my-4">
         Counter Rate Masyarakat Umum </p>
        <table className=" border-2 table-auto w-full text-center">
          <thead className="bg-blue-700 text-white">
            <tr className="border-2">
              <th rowSpan={2} className="border-2 text-lg font-bold px-4 py-2">Nominal</th>
              <th colSpan={5} className="border-2 text-lg font-bold px-4 py-2">Jangka Waktu</th>
            </tr>
            <tr>
              <th className="text-lg font-bold px-4 py-2">1</th>
              <th className="text-lg font-bold px-4 py-2">3</th>
              <th className="text-lg font-bold px-4 py-2">6</th>
              <th className="text-lg font-bold px-4 py-2">9</th>
              <th className="text-lg font-bold px-4 py-2">12</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-lg border px-4 py-2">{'<= 100 Juta'}</td>
              <td className="text-lg border px-4 py-2">4.75%</td>
              <td className="text-lg border px-4 py-2">5.25%</td>
              <td className="text-lg border px-4 py-2">5.75%</td>
              <td className="text-lg border px-4 py-2">6.00%</td>
              <td className="text-lg border px-4 py-2">6.50%</td>

            </tr>
            <tr>
            <td className="text-lg border px-4 py-2">{'>100 Juta - 1 Milliar'}</td>
              <td className="text-lg border px-4 py-2">5.00%</td>
              <td className="text-lg border px-4 py-2">5.50%</td>
              <td className="text-lg border px-4 py-2">6.00%</td>
              <td className="text-lg border px-4 py-2">6.25%</td>
              <td className="text-lg border px-4 py-2">6.75%</td>
            </tr>
            <tr>
            <td className="text-lg border px-4 py-2">{'> 1 Milliar'}</td>
              <td className="text-lg border px-4 py-2">5.25%</td>
              <td className="text-lg border px-4 py-2">5.75%</td>
              <td className="text-lg border px-4 py-2">6.25%</td>
              <td className="text-lg border px-4 py-2">6.50%</td>
              <td className="text-lg border px-4 py-2">6.75%</td>
            </tr>

          </tbody>
        </table>
        <p className="text-xl my-4">
         Counter Rate Lembaga Jasa Keuangan </p>
         <table className=" border-2 table-auto w-full text-center">
          <thead className="bg-blue-700 text-white">
            <tr className="border-2">
              <th colSpan={5} className="border-2 text-lg font-bold px-4 py-2">Jangka Waktu</th>
            </tr>
            <tr>
              <th className="text-lg font-bold px-4 py-2">1</th>
              <th className="text-lg font-bold px-4 py-2">3</th>
              <th className="text-lg font-bold px-4 py-2">6</th>
              <th className="text-lg font-bold px-4 py-2">9</th>
              <th className="text-lg font-bold px-4 py-2">12</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-lg border px-4 py-2">4.75%</td>
              <td className="text-lg border px-4 py-2">5.25%</td>
              <td className="text-lg border px-4 py-2">5.75%</td>
              <td className="text-lg border px-4 py-2">6.00%</td>
              <td className="text-lg border px-4 py-2">6.50%</td>

            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">5.00%</td>
              <td className="text-lg border px-4 py-2">5.50%</td>
              <td className="text-lg border px-4 py-2">6.00%</td>
              <td className="text-lg border px-4 py-2">6.25%</td>
              <td className="text-lg border px-4 py-2">6.75%</td>
            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">5.25%</td>
              <td className="text-lg border px-4 py-2">5.75%</td>
              <td className="text-lg border px-4 py-2">6.25%</td>
              <td className="text-lg border px-4 py-2">6.50%</td>
              <td className="text-lg border px-4 py-2">6.75%</td>
            </tr>

          </tbody>
        </table>
      </div>
    </section>
  )
}

export default DepositoTwo