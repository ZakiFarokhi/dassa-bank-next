'use client'
import { Tabs } from "antd"
import SectionTitle from "../Common/SectionTitle"
import Icon, { RocketOutlined } from "@ant-design/icons"
import { useState } from "react";
import BusinessOne from "./BusinessOne";
import BusinessTwo from "./BusinessTwo";
import DepositoOne from "./DepositoOne";
import DepositoTwo from "./DepositoTwo";
import KreditOne from "./KreditOne";
import KreditTwo from "./KreditTwo";

const { TabPane } = Tabs;
const Kredit = () => {
  const [activeKey, setActiveKey] = useState('1')
  const onKeyChange = (key) => setActiveKey(key)
  return (
    <section id="about" className="overflow-visible h-fit py-2 md:py-4 lg:py-8">
      <div className="h-full mb-20">

        <Tabs type="card" defaultActiveKey="1" size="large" activeKey={activeKey} onChange={onKeyChange}>
          <TabPane tab="Kredit" key="1" >
            <KreditOne />
          </TabPane>
          <TabPane tab="Suku Bunga" key="2"  >
            <KreditTwo />
          </TabPane>

        </Tabs>

        

      </div>


    </section>

  )
}


export default Kredit