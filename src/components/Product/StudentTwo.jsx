import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"

const StudentTwo = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className=" ">
        <SectionTitle title="Suku Bunga " paragraph="" dark={false} />
        <table className="table-auto w-2/3 text-center">
          <thead className="bg-blue-700 text-white">
            <tr>
              <th className="text-lg font-bold px-4 py-2">Tier</th>
              <th className="text-lg font-bold px-4 py-2">Suku Bunga</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-lg border px-4 py-2">s.d. 10jt</td>
              <td className="text-lg border px-4 py-2">s.d. 10jt</td>
            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">3.00 % p.a</td>
              <td className="text-lg border px-4 py-2">3.00 % p.a</td>
            </tr>

          </tbody>
        </table>
        <p className="text-xl my-4">
        Suku bunga tabungan adalah imbalan jasa yang diberikan bank atas sejumlah dana yang nasabah simpan di bank. Dapat dikatakan suku bunga tabungan merupakan hadiah yang diberikan pihak bank kepada nasabahnya.
Presentase suku bunga tabungan yang diterima pun berbeda-beda tergantung dari kebijakan masing-masing bank. Nominal yang akan didapatkan oleh nasabah pun berbeda tergantung dari jumlah saldo tabungan, jangka waktu serta presentase bunga. 
Terdapat 2 jenis suku bunga tabungan di bank di antaranya bunga tabungan konvensional dan bunga tabungan deposito. Bunga tabungan konvensional sendiri besarannya tidak terlalu besar, umumnya berlaku 1% per tahun. Jenis bunga ini diberikan oleh bank sebagai bentuk terimakasih kepada nasabah yang telah menggunakan jasa tabungan milik bank tersebut.  
         </p>

      </div>
    </section>
  )
}

export default StudentTwo