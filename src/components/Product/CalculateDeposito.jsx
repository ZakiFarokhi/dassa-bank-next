import React, { useState } from "react";
import { Button, Form, Input, Select, Modal } from "antd";

const CalculateDeposito = () => {
  const [result, setResult] = useState({
    totalBunga: "",
    saldoAkhir: "",
    nominalBeforeTax: "",
    pajak: ""
  });
  const [nominal, setNominal] = useState("");
  const [nominalBeforeTax, setNominalBeforeTax] = useState("");
  const [tenor, setTenor] = useState("");
  const [bunga, setBunga] = useState("");
  const [modalVisible, setModalVisible] = useState(false);

  const calculate = () => {
    const nominalFix = parseFloat(
      nominal.replace(/Rp\. |\./g, "").replace(",", ".")
    );

    let bungaFix = parseFloat(bunga) / 100;
    let tenorFix = parseFloat(tenor) * 30;
    let taxFix = 20 / 100;

    // Hitung jumlah bunga yang akan diterima
    const bungaPerBulan = nominalFix * (bungaFix / 365);
    const totalBunga = bungaPerBulan * tenorFix;

    // Hitung saldo akhir setelah dipotong pajak
    const saldoAkhir = nominalFix + totalBunga - totalBunga * taxFix;

    // Hitung pajak
    const pajak = totalBunga * taxFix;

    setResult({
      totalBunga: formatCurrency(totalBunga),
      saldoAkhir: formatCurrency(saldoAkhir),
      nominalBeforeTax: formatCurrency(nominalFix),
      pajak: formatCurrency(pajak)
    });
    setModalVisible(true);
  };

  const formatCurrency = (value) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  };

  const nominalChange = (e) => {
    const inputNominal = e.target.value;
    const formattedNominal = inputNominal
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    setNominal(formattedNominal);
  };

  const tenorChange = (value) => {
    setTenor(value);
  };

  const bungaChange = (value) => {
    setBunga(value);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  return (
    <div className="p-4 rounded-xl border-2 shadow-xl">
      <div className="rounded-xl text-center border-2 text-xl font-bold">
        Kalkulator Deposito
      </div>
      <Form
        layout="vertical"
        initialValues={{ size: "ka" }}
        style={{ maxWidth: 1000 }}
      >
        <Form.Item label="Nominal">
          <Input
            onChange={(e) => {
              nominalChange(e);
              setNominalBeforeTax(e.target.value);
            }}
            value={nominal}
            defaultValue={1000000000}
            size="large"
            className="bg-whitesmoke p-3 rounded-md"
            prefix="Rp. "
          />
        </Form.Item>
        <Form.Item label="Jangka Waktu (Tenor)">
          <Select value={tenor} onChange={tenorChange} className="p-3 rounded-md">
            <Select.Option value="3">3 Bulan</Select.Option>
            <Select.Option value="6">6 Bulan</Select.Option>
            <Select.Option value="9">9 Bulan</Select.Option>
            <Select.Option value="12">12 Bulan</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="Bunga">
          <Select value={bunga} onChange={bungaChange} className="p-3 rounded-md">
            <Select.Option value="6">6.00 %</Select.Option>
            <Select.Option value="6.50">6.50 %</Select.Option>
            <Select.Option value="5.75">5.75 %</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item className="text-center" label=" " colon={false}>
          <Button
            onClick={() => calculate()}
            shape="round"
            size="large"
            className="bg-blue-600 text-white mr-2"
            htmlType="submit"
          >
            Submit
          </Button>
          <Button shape="round" size="large" htmlType="submit" className="mr-2">
            Reset
          </Button>
        </Form.Item>
      </Form>
      <p className="mb-2">*tax 20%</p>
      <Modal
        title="Hasil simulasi perhitungan Deposito"
        visible={modalVisible}
        width={'700px'}
        onCancel={handleModalClose}
        footer={[
          <Button key="ok" onClick={handleModalClose} className="rounded-md">
            OK
          </Button>,
        ]}
      >
        <div className="p-4 mb-4 border rounded-md">
          <p className="text-dassaBlue text-lg font-ubuntu font-bold">Nominal Sebelum Pajak</p>
          <p className="text-dassaBlue text-xl underline font-ubuntu font-bold">{result.nominalBeforeTax}</p>
        </div>
        <div className="p-4 mb-4 border rounded-md">
          <p className="text-dassaBlue text-lg font-ubuntu font-bold">Jumlah Bunga</p>
          <p className="text-dassaBlue text-xl underline font-ubuntu font-bold">{result.totalBunga}</p>
        </div>
        <div className=" p-4 mb-4 border rounded-md">
          <p className="text-dassaBlue text-lg font-ubuntu font-bold">Pajak</p>
          <p className="text-dassaBlue text-xl underline font-ubuntu font-bold">{result.pajak}</p>
        </div>
        <div className=" p-4 mb-4 border rounded-md">
          <p className="text-dassaBlue text-lg font-ubuntu font-bold">Saldo Akhir</p>
          <p className="text-dassaBlue text-xl underline font-ubuntu font-bold">{result.saldoAkhir}</p>
        </div>
       
      </Modal>
    </div>
  );
};

export default CalculateDeposito;
