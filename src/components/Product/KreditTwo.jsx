import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"

const KreditTwo = () => {
  return (
    <section id="businessone" className=" overflow-hidden py-2 md:py-4 lg:py-8">
      <div className=" ">
        <SectionTitle title="Suku Bunga " paragraph="" dark={false} />
        <table className="table-auto w-full text-center">
          <thead className="bg-blue-700 text-white">
            <tr>
              <th className="text-lg font-bold px-4 py-2">Plafon</th>
              <th className="text-lg font-bold px-4 py-2">Kredit Modal Kerja</th>
              <th className="text-lg font-bold px-4 py-2">Kredit Multiguna/Investasi/Modal Kerja Tarm Loan</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-lg border px-4 py-2">500 Juta</td>
              <td className="text-lg border px-4 py-2">2.10 %</td>
              <td className="text-lg border px-4 py-2">1.50 %</td>
            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">{">500 Juta s.d. 1 Milyar"}</td>
              <td className="text-lg border px-4 py-2">1.90 %</td>
              <td className="text-lg border px-4 py-2">1.30 %</td>
            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">{">1 Milyar s.d. 2,5 Milyar"}</td>
              <td className="text-lg border px-4 py-2">1.85 %</td>
              <td className="text-lg border px-4 py-2">1.25 %</td>
            </tr>
            <tr>
              <td className="text-lg border px-4 py-2">{">2,5 Milyar"}</td>
              <td className="text-lg border px-4 py-2">1.80 %</td>
              <td className="text-lg border px-4 py-2">1.20 %</td>
            </tr>

          </tbody>
        </table>
        <ul className="list-decimal mx-2 my-4 w-2/3 text-xl">
          <li>Suku Bunga Dasar Kredit (SBDK) digunakan sebagai dasar penetapan suku bunga kredit yang akan dikenakan oleh Bank kepada nasabah. SBDK belum memperhitungkan komponen estimasi premi risiko yang besarnya tergantung dari penilaian Bank terhadap risiko untuk masing-masing debitur atau kelompok debitur. Dengan demikian, besarnya suku bungan kredit yang dikenakan kepada debitur belum tentu sama dengan SBDK. 
          </li>
          <li>SBDK dapat berubah sewaktu – waktu dan dapat dilihat informasinya pada website Bank BPR Universal.</li>
          <li>Untuk informasi lebih lanjut dapat menghubungi Universal Care 021-2221-3993</li>
        </ul>
      </div>
    </section>
  )
}

export default KreditTwo