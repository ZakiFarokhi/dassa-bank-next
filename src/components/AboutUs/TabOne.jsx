import Image from "next/image"
import SectionTitle from "../Common/SectionTitle"

const TabsOne = () => {
  return (
    <section id="tabone" className=" overflow-hidden ">
      <div className=" ">
        <SectionTitle title="Menuju Bank Dassa" paragraph="" dark={false} />
        <Image
        
          src="/images/about/tabs-tentang-dassa.webp"
          alt="cover"
          width={0}
          height={0}
          sizes="100vw"
          style={{ width: '100%', height: 'auto' }} // optional
        />
        <p className="mb-20 text-xl align-center">
        Bank Perekonomian Rakyat Dassa didirikan tahun 1991 sesuai akta pendirian nomor
68 tanggal 17 Juni 1991 yang dibuat dihadapan Notaris H. Asmawel Amin, SH, Notaris di
Jakarta dengan nama PT Bank Perkreditan Rakyat Pularta Mas dan telah mendapatkan
pesetujuan dari Menteri Kehakiman Republik Indonesia berdasarkan keputusan Menteri
Kehakiman Republik Indonesia Nomor C2-7105 HT.01.01.Th91 Tanggal 26 November 1991
dan pertama kali beroperasi pada tanggal 1 Juni 1992 dan berkantor di Jalan Komplek
Griya Kencana II blok B no.10 Ciledug, Tangerang Selatan Provinsi Banten. <br/><br/>
Tahun 2007 PT Bank Perkreditan Rakyat Pularta Mas mengalami perubahan nama
menjadi PT Bank Perkreditan Rakyat Pularta Mandiri berdasarkan Akta Nomor 9 Tanggal
5 Maret 2007 dibuat dihadapan Notaris H. Yanuardi, SH, Notaris di Jakarta dan telah
mendapatkan persetujuan dari Menteri Hukum dan Hak Asasi Manusia Republik
Indonesia sebagaimana tercantum dalam Keputusan Menteri Hukum dan Hak Asasi
Manusia Nomor W7-05084 HT.01.04-TH.2007 tanggal 4 Mei 2007. Perseroan telah
mendapatkan persetujuan dari Bank Indonesia, berdasarkan keputusan Direktur
Pengawasan Bank Perkreditan Rakyat Bank Indonesia Nomor 9/4/KEP.Dir.PBPR/2007
tanggal 5 Juni 2007. <br/><br/>
Tahun 2017 PT Bank Perkreditan Rakyat Pularta Mandiri diakuisisi oleh Titan Financial
Group dan berubah menjadi PT Bank Perkreditan Rakyat Dassa sesuai dengan Akta
Nomor 04 Tanggal 9 Februari 2017 yang dibuat dihadapan Notaris Dian Andiani, Sh dari
Menteri Hukum dan Hak Asasi Manusia berdasarkan keputusan Menteri Hukum dan
Hak Asasi Manusia Nomor AHU-0005626.AH.01.02.TAHUN 2017 tertanggal 7 Maret 2017
dan berlokasi kantor di Graha Anabatic Jalan Scientia Boulevard kav U2 Summarecon
Serpong Kabupaten Tangerang Propinsi Banten. <br/> <br/>
Perseroan mengalami pergantian pemilik, hal ini didasarkan Akta Nomor 02 Tanggal 13
April 2017 yang dibuat dihadapan Notaris Dian Andiani, SH.,M.Kn, Notaris di Kabupaten
Tangerang dan telah mendapatkan persetujuan dari Menteri Hukum dan Hak Asasi
Manusia berdasarkan Surat Penerimaan Pembertitahuan Perubahan Dasar Perseroan
Nomor AHU- AH.01.03.0131588 Tanggal 28 April 2017. <br/> <br/>
Perseroan telah mengalami beberapa kali perubahan anggaran dasar dan perubahan
anggaran dasar terakhir mengenai tingkat modal dasar dan modal disetor sebagaimana
tercantum dalam Akta Nomor 04 Tanggal 30 Juli 2018. Sesuai dengan anggaran dasar
perseroan, maksud dan tujuan didirikan perseroan adalah berusaha dalam bidang bank
perkreditan rakyat dengan melakukan kegiatan sebagai berikut: <br/> <br/>
a. Menghimpun dana dari masyarakat dalam bentuk tabungan dan deposito berjangka.<br/> <br/>
b.Memberikan kredit bagi para masyarakat khususnya pengusaha mikro, kecil dan
menengah.<br/> <br/>
Bank Dassa secara konsisten menjaga kesinambungan pertumbuhan usaha dan kualitas
kredit serta kinerja keuanga yang sehat dan optimal. Bank Dassa memberikan
kenyamanan dan keamanan Nasabah dalam melakukan transaksi keuangan dengan
dukungan IT yang handal. Dengan karyawan yang berintegritas dan profesional, Bank
Dassa berkomitmen untuk memberikan layanan prima kepada nasabah.
        </p>

      </div>
    </section>
  )
}

export default TabsOne