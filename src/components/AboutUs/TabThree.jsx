import { Image } from "antd";
import { Avatar, Card } from "antd";
import SectionTitle from "../Common/SectionTitle";

const TabThree = () => {
  return (
    <section id="tabtwo" className=" overflow-hidden">
      <div className=" ">
        <SectionTitle title="Filosofi" paragraph="" dark={false} />
        <Image src="/images/about/section/filosofi.webp" />

        {/* <div className="text-center bg-white my-8 p-8 rounded-xl shadow-xl">

          <Image
            src="/images/about/images-logo.png"
            alt="cover"
            width={0}
            height={0}
            sizes="100vw"
            style={{ margin: 'auto', textAlign: 'center', width: '200px', height: '200px' }} // optional
          />
          <p className="text-xl">Melambangkan Bank yang tumbuh dan berkembang didukung oleh kepercayaan Stakeholder</p>
          <div className=" my-8  p-8 rounded-xl shadow-xl">
            <div className="flex flex-row">
              <div className="mx-4 rounded-lg shadow-xl  basis-1/2 p-8 bg-gradient-to-r from-gray-100 to-gray-300">
                <div className="flex flex-row">
                  <div className="basis-1/6">
                    <div className=" w-20 h-20 bg-[#90C320] rounded-full"></div>
                  </div>
                  <div className="mx-4 border-l-2 basis-5/6 text-left">
                    <p className="text-2xl italic text-[#90C320] font-bold">Hijau</p>
                    <p className="text-lg">Melambangkan warna bumi, pertumbuhan dan kelimpahan</p>
                  </div>
                </div>
              </div>
              <div className="mx-4 rounded-lg shadow-xl basis-1/2 p-8 bg-gradient-to-r from-gray-100 to-gray-300">
                <div className="flex flex-row">
                  <div className="basis-1/6">
                  <div className="w-20 h-20 bg-[#0A51A1] rounded-full"></div>
                  </div>
                  <div className="mx-4 border-l-2 basis-5/6 text-left">
                    <p className="text-2xl italic text-[#0A51A1] font-bold">Biru</p>
                    <p className="text-lg">Melambangkan kebijakan, kepercayaan dan menentramkan</p>

                  </div>
                </div>
              </div>
            </div>

          
          </div>
        </div> */}
      </div>
    </section>
  );
};

export default TabThree;
