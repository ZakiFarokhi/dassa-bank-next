import { Row, Col, Card } from "antd"
import Image from "next/image"
import SectionTitle from "../Common/SectionTitle"
const { Meta } = Card

const TabSix = () => {
  return (
    <div>
    <SectionTitle title="Pemimpin Kami" paragraph="" dark={false} />

    <section id="our-leader">
      <Row>
        <Col className="px-8 my-2" span={8} >
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            
            cover={
              <Image
                src="/images/about/direksi/ONG-TEK-TJAN.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%',  borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/IMAM-R-SUHARTO.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
      </Row>

      <Row>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%' , borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/PDP.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        
      </Row>
      <Row>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%' , borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/FERRY.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        
      </Row>
      <Row>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/YUSNITA.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/RITA.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/SETIAWAN-SUSANTO.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
      </Row>
      <Row>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/RUDY.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/NITA.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/ARUM.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
      </Row>
      <Row>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/UCU.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/MAFATIHUL.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
        <Col className="px-8 my-2" span={8}>
          <Card
            hoverable
            style={{ width: '100%', borderRadius:'2%' }}
            cover={
              <Image
                src="/images/about/direksi/AMELIA.webp"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            {/* <Meta title="Christoper Riandi" description="Komisaris Utama" /> */}
          </Card>
        </Col>
      </Row>
    </section>
    </div>
  )
}

export default TabSix