import Image from "next/image"
import { Card } from "antd"
import { ArrowRightOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"

const CardReport = ({title,href}) => {
  return (
    <div className=" px-4 m-2 rounded shadow-lg justify-self-center">
      <Image src='/images/about/png-cover.jpg' width={200} height={200} alt="Card Image" />
      <div className="px-4 py-4">
      <div className="font-bold text-md mb-2">{title}</div>

      </div>
      <div className="px-4 py-1">
        <button className="font-semibold text-md  text-[#0A51A1] hover:bg-blue-700 hover:text-white border-2 border-[#0A51A1] py-2 px-4 rounded-full">
          Lihat PDF
        </button>
      </div>
    </div>
  )
}
const arr = []
const TabFive = () => {
  return (
    <section id="tabtwo" className=" overflow-hidden py-2">
      <div className=" ">
        <SectionTitle title="Laporan Tata Kelola" paragraph="" dark={false} />
        <div className="rounded-xl shadow-xl py-8 bg-white ">
          <div className="w-full bg-[#0A51A1] text-left p-4 rounded-md">
            <p className="text-2xl text-white font-bold">Laporan Tata Kelola</p>

          </div>
          <div className="grid grid-cols-4 gap-4">
            <CardReport title={"Laporan GCG"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
          </div>
          <div className="w-full bg-[#0A51A1] text-left p-4 rounded-md">
            <p className="text-2xl text-white font-bold">Laporan Keuangan</p>

          </div>
          <div className="grid grid-cols-4 gap-4">
            <CardReport title={"Laporan GCG"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
          </div>
          <div className="w-full bg-[#0A51A1] text-left p-4 rounded-md">
            <p className="text-2xl text-white font-bold">lainnya</p>
          </div>
          <div className="grid grid-cols-4 gap-4">
            <CardReport title={"Laporan GCG"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
            <CardReport title={"Lihat PDF"} href={'/'} />
          </div>
        </div>

      </div>
    </section>
  )
}

export default TabFive