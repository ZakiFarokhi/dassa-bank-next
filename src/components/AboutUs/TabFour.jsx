import {Image} from "antd"
import SectionTitle from "../Common/SectionTitle"

const TabFour = () => {
  return (
    <section id="tabFour" className="h-full overflow-visible ">
      <div className=" ">
        <SectionTitle title="Nilai Perusahaan" paragraph="" dark={false} />
        <Image src="/images/about/section/nilai-perusahaan.webp"/>

        {/* <div className="h-fit text-center bg-white mb-8 p-2 rounded-xl shadow-xl">

          <Image
            src="/images/about/image-about-infographic.png"
            alt="cover"
            width={0}
            height={0}
            sizes="100vw"
            style={{ margin: 'auto', textAlign: 'center', width: '500px', height: '800' }} // optional
          />
        
        </div> */}
      </div>
    </section>
  )
}

export default TabFour