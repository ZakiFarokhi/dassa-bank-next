"use client";
import { Tabs } from "antd";
import Icon, { RocketOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import TabsOne from "./TabOne";
import TabTwo from "./TabTwo";
import Hero from "./Hero";
import TabThree from "./TabThree";
import TabFour from "./TabFour";
import TabFive from "./TabFive";
import TabSix from "./TabSix";
import TabSeven from "./TabSeven";
import { useSearchParams } from "next/navigation";
import Breadcrumb from "../Common/Breadcrumb";
const { TabPane } = Tabs;
const Rocket = () => (
  <svg
    width="22"
    height="22"
    viewBox="0 0 22 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11 14L8 11M11 14C12.3968 13.4687 13.7369 12.7987 15 12M11 14V19C11 19 14.03 18.45 15 17C16.08 15.38 15 12 15 12M8 11C8.53214 9.61943 9.2022 8.29607 10 7.05C11.1652 5.18699 12.7876 3.65305 14.713 2.5941C16.6384 1.53514 18.8027 0.986375 21 1C21 3.72 20.22 8.5 15 12M8 11H3C3 11 3.55 7.97 5 7C6.62 5.92 10 7 10 7M3.5 15.5C2 16.76 1.5 20.5 1.5 20.5C1.5 20.5 5.24 20 6.5 18.5C7.21 17.66 7.2 16.37 6.41 15.59C6.02131 15.219 5.50929 15.0046 4.97223 14.988C4.43516 14.9714 3.91088 15.1537 3.5 15.5Z"
      stroke="#90C320"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Target = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M22 12H18M6 12H2M12 6V2M12 22V18M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM15 12C15 13.6569 13.6569 15 12 15C10.3431 15 9 13.6569 9 12C9 10.3431 10.3431 9 12 9C13.6569 9 15 10.3431 15 12Z"
      stroke="#90C320"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Hurricane = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M18 12C18 15.3137 15.3137 18 12 18C8.68629 18 6 15.3137 6 12M18 12C18 8.68629 15.3137 6 12 6C8.68629 6 6 8.68629 6 12M18 12C18 16.4183 14.4183 20 10 20C5.58172 20 2 16.4183 2 12M6 12C6 7.58172 9.58172 4 14 4C18.4183 4 22 7.58172 22 12M13 12C13 12.5523 12.5523 13 12 13C11.4477 13 11 12.5523 11 12C11 11.4477 11.4477 11 12 11C12.5523 11 13 11.4477 13 12Z"
      stroke="#90C320"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Share = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8.59 13.51L15.42 17.49M15.41 6.51L8.59 10.49M21 5C21 6.65685 19.6569 8 18 8C16.3431 8 15 6.65685 15 5C15 3.34315 16.3431 2 18 2C19.6569 2 21 3.34315 21 5ZM9 12C9 13.6569 7.65685 15 6 15C4.34315 15 3 13.6569 3 12C3 10.3431 4.34315 9 6 9C7.65685 9 9 10.3431 9 12ZM21 19C21 20.6569 19.6569 22 18 22C16.3431 22 15 20.6569 15 19C15 17.3431 16.3431 16 18 16C19.6569 16 21 17.3431 21 19Z"
      stroke="#90C320"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Report = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M14 2.26953V6.40007C14 6.96012 14 7.24015 14.109 7.45406C14.2049 7.64222 14.3578 7.7952 14.546 7.89108C14.7599 8.00007 15.0399 8.00007 15.6 8.00007H19.7305M8 15V18M16 13V18M12 10.5V18M20 9.98822V17.2C20 18.8802 20 19.7202 19.673 20.362C19.3854 20.9265 18.9265 21.3854 18.362 21.673C17.7202 22 16.8802 22 15.2 22H8.8C7.11984 22 6.27976 22 5.63803 21.673C5.07354 21.3854 4.6146 20.9265 4.32698 20.362C4 19.7202 4 18.8802 4 17.2V6.8C4 5.11984 4 4.27976 4.32698 3.63803C4.6146 3.07354 5.07354 2.6146 5.63803 2.32698C6.27976 2 7.11984 2 8.8 2H12.0118C12.7455 2 13.1124 2 13.4577 2.08289C13.7638 2.15638 14.0564 2.27759 14.3249 2.44208C14.6276 2.6276 14.887 2.88703 15.4059 3.40589L18.5941 6.59411C19.113 7.11297 19.3724 7.3724 19.5579 7.67515C19.7224 7.94356 19.8436 8.2362 19.9171 8.5423C20 8.88757 20 9.25445 20 9.98822Z"
      stroke="#90C320"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Leaders = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M22 21V19C22 17.1362 20.7252 15.5701 19 15.126M15.5 3.29076C16.9659 3.88415 18 5.32131 18 7C18 8.67869 16.9659 10.1159 15.5 10.7092M17 21C17 19.1362 17 18.2044 16.6955 17.4693C16.2895 16.4892 15.5108 15.7105 14.5307 15.3045C13.7956 15 12.8638 15 11 15H8C6.13623 15 5.20435 15 4.46927 15.3045C3.48915 15.7105 2.71046 16.4892 2.30448 17.4693C2 18.2044 2 19.1362 2 21M13.5 7C13.5 9.20914 11.7091 11 9.5 11C7.29086 11 5.5 9.20914 5.5 7C5.5 4.79086 7.29086 3 9.5 3C11.7091 3 13.5 4.79086 13.5 7Z"
      stroke="#9CBD64"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const Trophy = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12 15C8.68629 15 6 12.3137 6 9V3.44444C6 3.0306 6 2.82367 6.06031 2.65798C6.16141 2.38021 6.38021 2.16141 6.65798 2.06031C6.82367 2 7.0306 2 7.44444 2H16.5556C16.9694 2 17.1763 2 17.342 2.06031C17.6198 2.16141 17.8386 2.38021 17.9397 2.65798C18 2.82367 18 3.0306 18 3.44444V9C18 12.3137 15.3137 15 12 15ZM12 15V18M18 4H20.5C20.9659 4 21.1989 4 21.3827 4.07612C21.6277 4.17761 21.8224 4.37229 21.9239 4.61732C22 4.80109 22 5.03406 22 5.5V6C22 6.92997 22 7.39496 21.8978 7.77646C21.6204 8.81173 20.8117 9.62038 19.7765 9.89778C19.395 10 18.93 10 18 10M6 4H3.5C3.03406 4 2.80109 4 2.61732 4.07612C2.37229 4.17761 2.17761 4.37229 2.07612 4.61732C2 4.80109 2 5.03406 2 5.5V6C2 6.92997 2 7.39496 2.10222 7.77646C2.37962 8.81173 3.18827 9.62038 4.22354 9.89778C4.60504 10 5.07003 10 6 10M7.44444 22H16.5556C16.801 22 17 21.801 17 21.5556C17 19.5919 15.4081 18 13.4444 18H10.5556C8.59188 18 7 19.5919 7 21.5556C7 21.801 7.19898 22 7.44444 22Z"
      stroke="#9CBD64"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const RocketInactive = () => (
  <svg
    width="22"
    height="22"
    viewBox="0 0 22 22"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11 14L8 11M11 14C12.3968 13.4687 13.7369 12.7987 15 12M11 14V19C11 19 14.03 18.45 15 17C16.08 15.38 15 12 15 12M8 11C8.53214 9.61943 9.2022 8.29607 10 7.05C11.1652 5.18699 12.7876 3.65305 14.713 2.5941C16.6384 1.53514 18.8027 0.986375 21 1C21 3.72 20.22 8.5 15 12M8 11H3C3 11 3.55 7.97 5 7C6.62 5.92 10 7 10 7M3.5 15.5C2 16.76 1.5 20.5 1.5 20.5C1.5 20.5 5.24 20 6.5 18.5C7.21 17.66 7.2 16.37 6.41 15.59C6.02131 15.219 5.50929 15.0046 4.97223 14.988C4.43516 14.9714 3.91088 15.1537 3.5 15.5Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const TargetInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M22 12H18M6 12H2M12 6V2M12 22V18M20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12ZM15 12C15 13.6569 13.6569 15 12 15C10.3431 15 9 13.6569 9 12C9 10.3431 10.3431 9 12 9C13.6569 9 15 10.3431 15 12Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const HurricaneInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M18 12C18 15.3137 15.3137 18 12 18C8.68629 18 6 15.3137 6 12M18 12C18 8.68629 15.3137 6 12 6C8.68629 6 6 8.68629 6 12M18 12C18 16.4183 14.4183 20 10 20C5.58172 20 2 16.4183 2 12M6 12C6 7.58172 9.58172 4 14 4C18.4183 4 22 7.58172 22 12M13 12C13 12.5523 12.5523 13 12 13C11.4477 13 11 12.5523 11 12C11 11.4477 11.4477 11 12 11C12.5523 11 13 11.4477 13 12Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const ShareInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M8.59 13.51L15.42 17.49M15.41 6.51L8.59 10.49M21 5C21 6.65685 19.6569 8 18 8C16.3431 8 15 6.65685 15 5C15 3.34315 16.3431 2 18 2C19.6569 2 21 3.34315 21 5ZM9 12C9 13.6569 7.65685 15 6 15C4.34315 15 3 13.6569 3 12C3 10.3431 4.34315 9 6 9C7.65685 9 9 10.3431 9 12ZM21 19C21 20.6569 19.6569 22 18 22C16.3431 22 15 20.6569 15 19C15 17.3431 16.3431 16 18 16C19.6569 16 21 17.3431 21 19Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const ReportInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M14 2.26953V6.40007C14 6.96012 14 7.24015 14.109 7.45406C14.2049 7.64222 14.3578 7.7952 14.546 7.89108C14.7599 8.00007 15.0399 8.00007 15.6 8.00007H19.7305M8 15V18M16 13V18M12 10.5V18M20 9.98822V17.2C20 18.8802 20 19.7202 19.673 20.362C19.3854 20.9265 18.9265 21.3854 18.362 21.673C17.7202 22 16.8802 22 15.2 22H8.8C7.11984 22 6.27976 22 5.63803 21.673C5.07354 21.3854 4.6146 20.9265 4.32698 20.362C4 19.7202 4 18.8802 4 17.2V6.8C4 5.11984 4 4.27976 4.32698 3.63803C4.6146 3.07354 5.07354 2.6146 5.63803 2.32698C6.27976 2 7.11984 2 8.8 2H12.0118C12.7455 2 13.1124 2 13.4577 2.08289C13.7638 2.15638 14.0564 2.27759 14.3249 2.44208C14.6276 2.6276 14.887 2.88703 15.4059 3.40589L18.5941 6.59411C19.113 7.11297 19.3724 7.3724 19.5579 7.67515C19.7224 7.94356 19.8436 8.2362 19.9171 8.5423C20 8.88757 20 9.25445 20 9.98822Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const LeadersInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M22 21V19C22 17.1362 20.7252 15.5701 19 15.126M15.5 3.29076C16.9659 3.88415 18 5.32131 18 7C18 8.67869 16.9659 10.1159 15.5 10.7092M17 21C17 19.1362 17 18.2044 16.6955 17.4693C16.2895 16.4892 15.5108 15.7105 14.5307 15.3045C13.7956 15 12.8638 15 11 15H8C6.13623 15 5.20435 15 4.46927 15.3045C3.48915 15.7105 2.71046 16.4892 2.30448 17.4693C2 18.2044 2 19.1362 2 21M13.5 7C13.5 9.20914 11.7091 11 9.5 11C7.29086 11 5.5 9.20914 5.5 7C5.5 4.79086 7.29086 3 9.5 3C11.7091 3 13.5 4.79086 13.5 7Z"
      stroke="#FFFFFF"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
const TrophyInactive = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12 15C8.68629 15 6 12.3137 6 9V3.44444C6 3.0306 6 2.82367 6.06031 2.65798C6.16141 2.38021 6.38021 2.16141 6.65798 2.06031C6.82367 2 7.0306 2 7.44444 2H16.5556C16.9694 2 17.1763 2 17.342 2.06031C17.6198 2.16141 17.8386 2.38021 17.9397 2.65798C18 2.82367 18 3.0306 18 3.44444V9C18 12.3137 15.3137 15 12 15ZM12 15V18M18 4H20.5C20.9659 4 21.1989 4 21.3827 4.07612C21.6277 4.17761 21.8224 4.37229 21.9239 4.61732C22 4.80109 22 5.03406 22 5.5V6C22 6.92997 22 7.39496 21.8978 7.77646C21.6204 8.81173 20.8117 9.62038 19.7765 9.89778C19.395 10 18.93 10 18 10M6 4H3.5C3.03406 4 2.80109 4 2.61732 4.07612C2.37229 4.17761 2.17761 4.37229 2.07612 4.61732C2 4.80109 2 5.03406 2 5.5V6C2 6.92997 2 7.39496 2.10222 7.77646C2.37962 8.81173 3.18827 9.62038 4.22354 9.89778C4.60504 10 5.07003 10 6 10M7.44444 22H16.5556C16.801 22 17 21.801 17 21.5556C17 19.5919 15.4081 18 13.4444 18H10.5556C8.59188 18 7 19.5919 7 21.5556C7 21.801 7.19898 22 7.44444 22Z"
      stroke="#FFFFFF"
      stroke-width="2"
      stroke-linecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const urls = [
  "/images/about/about-hero-1.png",
  "/images/about/about-hero-2.png",
  "/images/about/about-hero-3.png",
  "/images/about/about-hero-4.png",
  "/images/about/about-hero-5.png",
  "/images/about/about-hero-6.png",
  "/images/about/about-hero-6.png",
];
const title = [
  'Menuju Bank DASSA',
  'Visi dan Misi',
  'Filosofi',
  'Nilai Perusahaan',
  'Pemimpin Kami',
  'Penghargaan',
  'Laporan Tata Kelola'
]
const AboutUs = () => {
  const searchParams = useSearchParams();

  const search = searchParams.get("search");
  const id = searchParams.get("id");
  console.log(id);
  const [activeKey, setActiveKey] = useState(id);
  const onKeyChange = (key) => setActiveKey(key);
  useEffect(() => {
    setActiveKey(id);
  }, []);
  return (
    <section id="about" className="overflow-visible ">
      <Hero url={urls[activeKey - 1]} title={title[activeKey - 1]} />
      <div className="container mx-auto my-8">
        <Breadcrumb pageName="About Us" description="" />
        <div className="sm:block md:block lg:hidden h-fullc mb-20">
          <Tabs
            tabPosition="top"
            size="large"
            defaultActiveKey="1"
            activeKey={activeKey}
            onChange={onKeyChange}
          >
            <TabPane
              tab="Menuju Bank DASSA"
              key="1"
              icon={
                <Icon component={activeKey === "1" ? Rocket : RocketInactive} />
              }
            >
              <TabsOne />
            </TabPane>
            <TabPane
              tab="Visi dan Misi"
              key="2"
              icon={
                <Icon component={activeKey === "2" ? Target : TargetInactive} />
              }
            >
              <TabTwo />
            </TabPane>
            <TabPane
              tab="Filosofi"
              key="3"
              icon={
                <Icon
                  component={activeKey === "3" ? Hurricane : HurricaneInactive}
                />
              }
            >
              <TabThree />
            </TabPane>
            <TabPane
              tab="Nilai Perusahaan"
              key="4"
              icon={
                <Icon component={activeKey === "4" ? Share : ShareInactive} />
              }
            >
              <TabFour />
            </TabPane>
            <TabPane
              tab="Pemimpin Kami"
              key="6"
              icon={
                <Icon
                  component={activeKey === "6" ? Leaders : LeadersInactive}
                />
              }
            >
              <TabSix />
            </TabPane>
            <TabPane
              tab="Penghargaan"
              key="7"
              icon={
                <Icon component={activeKey === "7" ? Trophy : TrophyInactive} />
              }
            >
              <TabSeven />
            </TabPane>
            <TabPane
              tab="Laporan Tata Kelola"
              key="5"
              icon={
                <Icon component={activeKey === "5" ? Report : ReportInactive} />
              }
            >
              <div className="flex">
                <TabFive />
              </div>
            </TabPane>
          </Tabs>
        </div>
        <div className="hidden xl:block lg:block h-full mb-20">
          <Tabs
            tabPosition="left"
            size="large"
            defaultActiveKey="1"
            activeKey={activeKey}
            onChange={onKeyChange}
          >
            <TabPane
              tab="Menuju Bank DASSA"
              key="1"
              icon={
                <Icon component={activeKey === "1" ? Rocket : RocketInactive} />
              }
            >
              <TabsOne />
            </TabPane>
            <TabPane
              tab="Visi dan Misi"
              key="2"
              icon={
                <Icon component={activeKey === "2" ? Target : TargetInactive} />
              }
            >
              <TabTwo />
            </TabPane>
            <TabPane
              tab="Filosofi"
              key="3"
              icon={
                <Icon
                  component={activeKey === "3" ? Hurricane : HurricaneInactive}
                />
              }
            >
              <TabThree />
            </TabPane>
            <TabPane
              tab="Nilai Perusahaan"
              key="4"
              icon={
                <Icon component={activeKey === "4" ? Share : ShareInactive} />
              }
            >
              <TabFour />
            </TabPane>
            <TabPane
              tab="Pemimpin Kami"
              key="6"
              icon={
                <Icon
                  component={activeKey === "6" ? Leaders : LeadersInactive}
                />
              }
            >
              <TabSix />
            </TabPane>
            <TabPane
              tab="Penghargaan"
              key="7"
              icon={
                <Icon component={activeKey === "7" ? Trophy : TrophyInactive} />
              }
            >
              <TabSeven />
            </TabPane>
            <TabPane
              tab="Laporan Tata Kelola"
              key="5"
              icon={
                <Icon component={activeKey === "5" ? Report : ReportInactive} />
              }
            >
              <TabFive />
            </TabPane>
          </Tabs>
        </div>
        {/* <div className="">
          <Tabs
            tabPosition={"left"}
            size="large"
            defaultActiveKey="1"
            activeKey={activeKey}
            onChange={onKeyChange}
            className="text-left"
          >
            <TabPane
              tab="Menuju Bank DASSA"
              key="1"
              icon={
                <Icon component={activeKey === "1" ? Rocket : RocketInactive} />
              }
            >
              <TabsOne />
            </TabPane>
            <TabPane
              tab="Visi dan Misi"
              key="2"
              icon={
                <Icon component={activeKey === "2" ? Target : TargetInactive} />
              }
            >
              <TabTwo />
            </TabPane>
            <TabPane
              tab="Filosofi"
              key="3"
              icon={
                <Icon
                  component={activeKey === "3" ? Hurricane : HurricaneInactive}
                />
              }
            >
              <TabThree />
            </TabPane>
            <TabPane
              tab="Nilai Perusahaan"
              key="4"
              icon={
                <Icon component={activeKey === "4" ? Share : ShareInactive} />
              }
            >
              <TabFour />
            </TabPane>
            <TabPane
              tab="Pemimpin Kami"
              key="6"
              icon={
                <Icon
                  component={activeKey === "6" ? Leaders : LeadersInactive}
                />
              }
            >
              <TabSix />
            </TabPane>
            <TabPane
              tab="Penghargaan"
              key="7"
              icon={
                <Icon component={activeKey === "7" ? Trophy : TrophyInactive} />
              }
            >
              <TabSeven />
            </TabPane>
            <TabPane
              tab="Laporan Tata Kelola"
              key="5"
              icon={
                <Icon component={activeKey === "5" ? Report : ReportInactive} />
              }
            >
              <TabFive />
            </TabPane>
          </Tabs>
        </div> */}
      </div>
    </section>
  );
};

export default AboutUs;
