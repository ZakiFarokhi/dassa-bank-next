import { Image } from "antd";
import SectionTitle from "../Common/SectionTitle";

const TabTwo = () => {
  return (
    <section id="tabtwo" className=" overflow-hidden ">
      <div className=" ">
        <SectionTitle title="Visi dan Misi" paragraph="" dark={false} />
        <Image src="/images/about/section/visi-misi.webp"/>
        {/* <div className="bg-pattern h-screen"></div> */}

        {/* <div className="bg-gradient-to-r from-gray-300 to-gray-100 my-8 p-8 rounded-xl shadow-xl">
          <Image
            src="/images/about/icon-eye.png"
            alt="cover"
            width={0}
            height={0}
            sizes="100vw"
            style={{ width: '90px', height: '90px' }} // optional
          />
          <p className="text-xl">Menjadi BPR TERBAIK dan TERBESAR di Provinsi Banten</p>
        </div>
        <div className="bg-gradient-to-r from-gray-300 to-gray-100 my-8 bg-gray-200 p-8 rounded-xl shadow-xl">
          <Image
            src="/images/about/icon-target.png"
            alt="cover"
            width={0}
            height={0}
            sizes="100vw"
            style={{ width: '90px', height: '90px' }} // optional
          />
          <ul className="list-disc">
            <li className="text-lg">Secara konsisten menjaga kesinambungan pertumbuhan usaha dan kualitas kredit serta kinerja keuangan yang sehat dan optimal, guna memberikan nilai tambah bagi stakeholders</li>
            <li className="text-lg">Memberikan kenyamanan dan keamanan Nasabah dalam melakukan transaksi keuangan dengan dukungan IT yang handal</li>
            <li className="text-lg">Karyawan yang berintegritas dan professional, berkomitmen untuk memberikan layanan prima kepada Nasabah serta praktek perbankan yang prudent</li>
            <li className="text-lg">Melakukan penerapan Tata Kelola, Manajement Resiko dan Prinsip kehati-hatian secara konsisten, dinamis dan berkesinambungan</li>
          </ul>
        </div> */}
      </div>
    </section>
  );
};

export default TabTwo;
