import { Row, Col, Card } from "antd"
import Image from "next/image"
import SectionTitle from "../Common/SectionTitle"
const { Meta } = Card

const TabSeven = () => {
  return (
    <div>
    <SectionTitle title="Penghargaan" paragraph="" dark={false} />

    <section id="our-trophy">
      <Row>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
        <Col span={8} className="px-8 py-4">
          <Card
            hoverable
            style={{ width: '100%' }}
            cover={
              <Image
                src="/images/about/trophy.png"
                alt="cover"
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '100%', height: 'auto' }} // optional
              />
            }
          >
            <Meta title="Christoper Riandi" description="Komisaris Utama" />
          </Card>
        </Col>
      </Row>
      

      
    </section>
    </div>
  )
}

export default TabSeven