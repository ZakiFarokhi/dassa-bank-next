import Image from "next/image";
import CardNews from "./CardNews";
import { Button, Input } from "antd";
import { InfoCircleOutlined, SearchOutlined } from "@ant-design/icons";
import Link from "next/link";
import SideNavItems from "@/components/News/SideNavItems";

const SideNav = () => {
  return (
    <section className="overflow-hidden py-2 md:py-2 lg:py-2 ">
      <div>
        <Input style={{ backgroundColor: "whitesmoke" }} size="large" placeholder="Input text" prefix={<SearchOutlined />} />

        <h2 className=" py-4 underline decoration-sky-500 mb-3 text-2xl font-bold text-black dark:text-white sm:text-3xl lg:text-2xl xl:text-3xl">
          Kategori &nbsp;
        </h2>
        <div className="flex justify-between overflow-x-scroll">
          <button className="text-sm rounded-full border border-green-500 text-green-500 px-2 py-1 hover:bg-green-500 hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
            Press Release
          </button>
          <button className="text-sm rounded-full border border-green-500 text-green-500 px-2 py-1 hover:bg-green-500 hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
            Management
          </button>
          <button className="text-sm rounded-full border border-green-500 text-green-500 px-2 py-1 hover:bg-green-500 hover:text-white hover:border-green-700 transition duration-300 ease-in-out">
            Investasi
          </button>
        </div>
        <h2 className=" py-4 underline decoration-sky-500 mb-3 text-2xl font-bold text-black dark:text-white sm:text-2xl lg:text-2xl xl:text-2xl">
          Berita Terkait &nbsp;
        </h2>

        <SideNavItems images={"/images/news/images-side-nav-items-5.webp"} title={"Kemeriahan Peringatan Kemerdekaan Indonesia ke 78"} date={"18 Aug, 2023"} />
        <SideNavItems images={"/images/news/images-side-nav-items-6.webp"} title={"Kredit kepemilikan ruang dagang di Pasar Kemiri"} date={"02 Mar, 2022"} />
        <SideNavItems images={"/images/news/images-side-nav-items-7.webp"} title={"Outing Bank Dassa : Ready to the Next Level"} date={"15 Oct, 2023"} />
        <SideNavItems images={"/images/news/images-side-nav-items-8.webp"} title={"Bank Dassa bangun kerjasama dengan Prodigi / Broker Asuransi"} date={"12 Dec, 2023"} />
        <Link href={`/allnews`}>
          <div className="text-center rounded-full w-full bg-dassaGreen  hover:bg-blue-700 text-sm">
            <button type="submit" className="w-full font-semibold text-lg  text-black bg-white pt-2 pb-3 px-8 rounded-full">
              Lainnya &nbsp;
            </button>
          </div>
        </Link>
      </div>
    </section>
  )
}

export default SideNav
