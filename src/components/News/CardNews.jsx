import { ArrowRightOutlined, ForwardOutlined, ShareAltOutlined } from "@ant-design/icons";
import { Card } from "antd";
import Image from "next/image";
import Link from "next/link";
const { Meta } = Card

export default function CardNews({ images, title, description, id }) {
  return (
    <div className="w-full px-4 py-4  rounded-xl overflow shadow-lg justify-self-center pt-1">
      <Image src={ images} width={850} height={500} alt="Card Image" className="rounded-xl" />
      <div className="px-4 py-4">
        <div className="font-bold text-2xl mb-2">{title}</div>
        <p className="text-gray-700 text-base">{description}</p>
      </div>
      <div className="px-4 py-4 flex justify-between">
        <Link href={`/news/${id}`}>
          <button className="font-semibold bg-dassaGreen  hover:bg-blue-700 text-sm text-white py-3 px-8 rounded-full">
            Selengkapnya &nbsp;
            <ArrowRightOutlined style={{ fontSize: 20 }} />
          </button>
        </Link>

        <ShareAltOutlined size={64} style={{ color: "GrayText" }} />
      </div>
    </div>
  )
}