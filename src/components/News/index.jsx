import Image from "next/image";
import CardNews from "./CardNews";
import SideNav from "./SideNav";

const News = (props) => {
  // console.log(props.blogs)
  return (
    <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <h2 className=" underline decoration-sky-500 mb-3 text-2xl font-bold text-black dark:text-white sm:text-3xl lg:text-2xl xl:text-3xl">
          Berita &nbsp;
        </h2>
        <div className="-mx-4 flex flex-wrap py-4 rounded">
          <div className="w-full px-4 lg:w-3/4 xl:w-2/3 flex flex-col justify-center items-center">
            <div className="py-2" key={1}>
              <CardNews
                key={3}
                id={3}
                title={"Kemeriahan Peringatan Kemerdekaan Indonesia ke 78"}
                images={'/images/news/images-news-3.webp'}
                description={"Peringatan Hari Kemerdekaan Republik Indonesia yang ke 78 berlangsung dengan seru dan penuh kekeluargaan."}
              />
            </div>
            <div className="py-2" key={2}>
              <CardNews
                key={4}
                id={4}
                title={"Kredit kepemilikan ruang dagang di Pasar Kemiri"}
                images={'/images/news/images-news-4.webp'}
                description={"Pasar Kemiri adalah satu-satunya Pasar yang berada di Kecamatan Kemiri Tangerang. "}
              />
            </div>            
          </div>
          <div className="w-full h-screen px-4 lg:w-1/4 xl:w-1/3">
            <div
              className="mb-12 rounded-sm bg-white px-2 shadow-three dark:bg-gray-dark sm:p-4 lg:mb-5 lg:px-4 xl:p-4"
              data-wow-delay=".15s"
            >
              <SideNav />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default News;
