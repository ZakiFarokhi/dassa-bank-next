import { ClockCircleOutlined } from "@ant-design/icons";
import Image from "next/image";

export default function SideNavItems({ images, title, date }) {
  return (
    <div className="flex justify-between p-4 border-b-2">
      <div className="flex w-1/3 px-2 rounded ">
        <Image src={images} width={100} height={100} alt="images-side-nav" />
      </div>
      <div className="w-2/3">
        <p className="text-lg font-bold">{title}</p>
        <p ><ClockCircleOutlined style={{color:"#52c41a"}} /> &nbsp; {date}</p>
      </div>
    </div>
  )
}