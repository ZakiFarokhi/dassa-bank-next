import Image from "next/image";
import CardNews from "./CardNews";
import SideNav from "./SideNav";
import {
  FacebookFilled,
  LinkedinFilled,
  WechatFilled,
  WhatsAppOutlined,
  XFilled,
  XOutlined,
} from "@ant-design/icons";

const NewsDetail = async ({ id }) => {
  const data = [
    {
      id: 1,
      image: "/images/news/images-side-nav-items-8.webp",
      title: "Bank Dassa bangun kerjasama dengan Prodigi / Broker Asuransi",
      description:
        "PT Bank Perekonomian Rakyat Dassa (Bank Dassa) Kerjasama dengan PT Perdana Wahana Sentosa (Prodigi) / Broker Asuransi",
      body: "Sebuah Langkah Maju Menuju Layanan Keuangan yang Lebih Baik. Kerjasama antara Bank Dassa dan Sekitarnya dengan Prodigi merupakan langkah maju yang signifikan dalam upaya meningkatkan layanan keuangan bagi nasabah Bank Dassa. Kolaborasi ini menghadirkan berbagai manfaat bagi para nasabah Bank Dassa, termasuk: 1. Memberikan Rekomendasi Produk Asuransi dengan Manfaat Terbaik Nasabah Bank Dassa kini dapat dengan mudah mendapatkan akses ke berbagai produk asuransi melalui Prodigi . Hal ini akan membantu mereka dalam memilih produk asuransi yang tepat untuk kebutuhan dan profil risikonya. 2. Konsultasi dan Pendampingan Profesional Prodigi menyediakan tim profesional yang siap membantu nasabah Bank Dassa dalam memahami produk asuransi dan memilih solusi terbaik. Tim ini juga akan membantu nasabah dalam proses klaim asuransi, sehingga mereka dapat dengan mudah mendapatkan manfaat yang dijanjikan. 3. Memiliki Keahlian dan Pengetahuan yang Memadai sehingga bisa memberikan literasi asuransi Kerjasama ini diharapkan dapat meningkatkan literasi asuransi di kalangan nasabah Bank Dassa.",
    },
    {
      id: 2,
      image: "/images/news/images-side-nav-items-9.webp",
      title: "Kerjasama dengan Openbank+",
      description:
        "Openbank+ adalah sebuah sistem platform yang menyediakan produk dan layanan bekerjasama dengan bank umum dan penyedia layanan pihak ketiga",
        body: "memanfaatkan konsep open banking dan teknologi open API yang tersedia. Dengan demikian, memudahkan pengguna untuk terhubung langsung dengan bank umum dan penyedia layanan pihak ketiga untuk proses transaksi, baik finansial maupun non finansial. OpenBank+ dirancang untuk mendukung Bank Dassa dalam program transformasi ke digitalisasi perbankan dan merupakan cara aman dan cepat untuk BPR memberikan berbagai produk dan layanan ke nasabah dan pebisnis Kecil dan Menengah"
    },
    {
      id: 3,
      image: "/images/news/images-side-nav-items-5.webp",
      title: "Kemeriahan Peringatan Kemerdekaan Indonesia ke 78",
      description:
        "Peringatan Hari Kemerdekaan Republik Indonesia yang ke 78 berlangsung dengan seru dan penuh kekeluargaan",
        body:"Peringatan Hari Kemerdekaan Republik Indonesia yang ke 78 berlangsung dengan seru dan penuh kekeluargaan. Acara tahunan yang selalu diperingati oleh semua karyawan Bank Dassa ini tepat dilaksanakan tanggal 18 Agustus 2023 atau tepat sehari setelah peringatan kemerdekaan. Walaupun dengan kondisi ruang yang terbatas, acara peringatan kemerdekaan tetap berlangung meriah dan dilakukan beberapa perlombaan diantaranya, lomba pecah balon, lomba memasukan kelereng ke dalam toples, lomba estafet memindahkan sedotan dan perlombaan lainnya. Semua ikut terlibat dalam ikut aktif dalam acara ini yang menciptakan kekompakan dan diharapkan berdampak pada kegiatan bekerja sehari-hari. Meskipun dengan hadiah seadanya, namun acara ini sangat berkesan dan menumbuhkan sikap kerjasama dan kekerabatan antar karyawan dan tentunya selalu bisa menambah nasionalisme."
    },
    {
      id: 4,
      image: "/images/news/images-side-nav-items-6.webp",
      title: "Kredit kepemilikan ruang dagang di Pasar Kemiri",
      description:
        "Pasar Kemiri adalah satu-satunya Pasar yang berada di Kecamatan Kemiri Tangerang. ",
        body: "Pasar Kemiri adalah satu-satunya Pasar yang berada di Kecamatan KemiriTangerang. Beroperasi dari mulai dini hari sampai sekitar jam 09 pagi, kesibukan Pasar Kemiri terlihat jelas saat kami tim Bank Dassa sampai disana.Jarak tempuh sekitar 36 km tidak mengurangi semangat tim Bank Dassa untuk mengunjungi pedagang-pedagang di Pasar Kemiri untuk memberikan solusi kredit kepemilikan ruang dagang / kios untuk pedagang disana.Setelah mengalamai revitalisasi kondisi Pasar Kemiri memang terlihat lebih teratur dari sebelumnya.26 September 2023 merupakan hari yang penting bagi Bank Dassa. Untuk pertama kalinya Bank Dassa melakukan kredit kepemilikan kios / ruang usaha untuk para pedagang di Pasar Kemiri. Terlihat antusiasme para pedagang untukmengikuti acara yang diadakan oleh Bank Dassa"
    },
    {
      id: 5,
      image: "/images/news/images-side-nav-items-7.webp",
      title: "Outing Bank Dassa : Ready to the Next Level",
      description:
        "Ready to the next level, itulah semangat Bank Dassa yang baru untuk menghadapi tantangan perbankan.",
         body:"eady to the next level, itulah semangatBank Dassa yang baru untukmenghadapi tantangan perbankanapalagi di era digitalisasi seperti saat ini.Semangat ini yang akan Tim Dassa bawadalam kehidupan sehari-hari, denganadanya outing tahunan ini diharapkanmembawa semangat dan dampak positifdemi mewujudkan cita-cita Bank Dassamenuju Go Digital.Outing tahun ini dilaksanakan di The Lodge Camp Village, Maribaya Bandung. Keseruan outing kali ini dilaksanakan tanggal 15 Oktober 2023 dimana seluruh karyawan bank Dassa bertolak ke lokasi outing dari jam 06 pagi. Tampak antusiasme dari seluruh karyawan Bank Dassa walaupun harus bangun pagi di hari libur untuk berangkat langsung ke lokasi Outing.Sesampainya di lokasi, Tim Bank Dassa langsung ke tempat wisata The Lodge Maribaya, dan menikmati dingin dan hijaunya pepohonan disekitar kawasan tidak lupa foto-foto dan membuat konten Reels untuk kompetisi Reels Competition antar karyawan, terlihat kreativitas karyawan yang selama ini jarang terlihat. Semua sangat bahagia dan excited."
    },
    {
      id: 6,
      image: "/images/news/images-side-nav-items-10.webp",
      title: "Bank Dassa gelar Dassa Cup untuk pertama kalinya",
      description:"Bank Dassa untuk pertama kalinya menggelar turnamen bulutangkis antar sesama karyawan Bank Dassa.",
      body: "Bank Dassa untuk pertama kalinyamenggelar turnamen bulutangkisantar sesama karyawan Bank Dassa.Turnamen ini bertajuk Dassa Cup2023 dan diikuti oleh 16 peserta dan digelar pada 26 April 2023.Dassa Cup pertama ini akan diagenda kan menjadi turnamen tahunan yangdigelar Bank Dassa. Melalui ajangolahraga ini diharapkan bisa menjalin kekompakan antar sesama karyawan.Direktur Utama Bank Dassa, Pahala D Pandjaitan, memberikan dukungan penuh terkait penyelenggaraan Dassa Cup 2023. Pahala D Pandjaitan berharap melalui turnamen ini sekaligus bisa meningkatkan hubungan baik antar sesama karyawan.Hal senada juga diungkapkan oleh Rudy Ilham yang didapuk sebagai ketua panitia pada Dassa Cup 2023, Rudy Ilham menilai ajang ini menjadi momentum para karyawan untuk merasakan atmosfer pertandingan bak pemain bulutangkis profesional.Turnamen ini hanya terdiri dari partai Ganda Putra, Putri dan Ganda Campuran. dan untuk Dassa Cup pertama ini dimenangkan oleh pasangan Natasya Yemima dan Arum Dwi Chrisno Rini"
    },
  ];

  return (
    <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <h2 className="  mb-3 text-2xl font-bold text-black dark:text-white sm:text-2xl lg:text-2xl xl:text-2xl">
          {data.find((item) => item.id == id).title}
        </h2>
        <div className="-mx-4 flex flex-wrap py-4 rounded">
          <div className="w-full px-4 lg:w-3/4 xl:w-2/3 flex flex-col justify-center items-center">
            <div className="py-2">
              <Image
                src={data.find((item) => item.id == id).image}
                width={850}
                height={500}
                alt="Card Image"
              />
              <div className=" py-4">
                <div className="text-gray-700 text-md font-semibold">
                  {data.find((item) => item.id == id).description}
                </div>
                {data.find((item) => item.id == id).body}
              </div>
              <div className="px-4 py-4">
                <p className="text-gray-700 text-base">share</p>
                <div className="flex">
                  <LinkedinFilled
                    style={{
                      color: "blue",
                      fontSize: "20px",
                      marginRight: "10px",
                    }}
                  />
                  <FacebookFilled
                    style={{
                      color: "blue",
                      fontSize: "20px",
                      marginRight: "10px",
                    }}
                  />
                  <XOutlined
                    style={{ fontSize: "20px", marginRight: "10px" }}
                  />
                  <WhatsAppOutlined
                    style={{
                      color: "green",
                      fontSize: "20px",
                      marginRight: "10px",
                    }}
                  />
                </div>
              </div>{" "}
            </div>
          </div>
          <div className="w-full h-screen px-4 lg:w-1/4 xl:w-1/3">
            <div
              className="mb-12 rounded-sm bg-white px-2 shadow-three dark:bg-gray-dark sm:p-4 lg:mb-5 lg:px-4 xl:p-4"
              data-wow-delay=".15s"
            >
              <SideNav />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NewsDetail;
