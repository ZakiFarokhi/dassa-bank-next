import Image from "next/image";
import { ArrowRightOutlined } from "@ant-design/icons";
import FormApply from "./Apply";

const CareerDetail = async ({ id }) => {
  return (
    <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <div className="mx-20 ">
          <p className=" my-4 text-4xl font-bold text-black dark:text-white sm:text-2xl lg:text-4xl xl:text-4xl">
            Finance Staff &nbsp;
          </p>
          <p className="my-4 text-gray-700 text-base">{`Jakarta | Permanen | WFO`}</p>
          <div className="my-4 text-black text-2xl font-semibold">
            Job Description
          </div>
          <div className="text-xl font-ubuntu ">
            As a Finance Staff (Accounts Payable Officer), you will be
            responsible in managing that invoices are processed and paid on time
            and with high quality and according to Invoice to Payment process
            and approved legal requirements.
          </div>
          <div className="my-4 text-black text-2xl font-semibold">
            Qualification
          </div>
          <div className="text-xl leading-8 whitespace-pre-line list-decimal">
            Female Max. 30 years old <br />
            Min. Diploma / Bachelor’s degree in Accounting from reputable
            university
            <br />
            MUST have at least one (1) year working experiences in the same
            position <br />
            Detail oriented with excellent interpersonal, organizational, and
            analytical skills <br />
            Ability to effectively communicate with all levels of internal staff
            as well as external parties and executive management;Computer
            literacy is a must <br />
            Sound knowledge of Accounting software and ERP system would be a
            plus point <br />
          </div>
          
          <div className="px-4 py-4">
            <FormApply />
            {/* <button className="font-semibold text-lg bg-gradient-to-r from-green-400 to-lime-400  hover:bg-blue-700 text-sm text-white py-3 px-8 rounded-full">
              Lamar &nbsp;
              <ArrowRightOutlined style={{ fontSize: 20 }} />
            </button> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default CareerDetail;
