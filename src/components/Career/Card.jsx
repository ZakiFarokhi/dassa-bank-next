import { InfoCircleOutlined } from "@ant-design/icons";
import moment from "moment";
import "moment/locale/id";
import Link from "next/link";

const CardCareer = ({ id, position, location, dueDate }) => {
  const duDate = moment(dueDate);
  duDate.locale("id");

  return (
    <section className="">
      <div className="w-full my-4 p-4 rounded-2xl border-2 bg-white shadow-xl">
        <div className="flex">
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1 text-base">Posisi</label>
            <p className="text-lg font-semibold">{position}</p>
          </div>
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1 text-base">Lokasi</label>
            <p className="text-lg font-semibold">{location}</p>
          </div>
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1 text-base">Batas Lamaran</label>
            <p className="text-lg font-semibold">{duDate.format("LL")}</p>
          </div>
          <div className="text-center px-1 md:w-1/4 self-end text-sm">
          <Link href={`/career/${id}`}>
            {/* <div className="text-center w-full px-2 md:w-1/4 self-end"> */}
              <button
                type="submit"
                className=" font-semibold  bg-white border-dassaGreen border-2 hover:bg-[#90C320]/20 text-md text-dassaGreen py-2 px-4 rounded-full"
              >
                Detail &nbsp;
                <InfoCircleOutlined style={{ fontSize: 20 }} />
              </button>
            {/* </div> */}
            {/* <div className="text-center rounded-full w-fit bg-gradient-to-r p-1 from-green-400 to-lime-400  hover:bg-blue-700 text-sm">
              <button
                type="submit"
                className=" font-semibold  bg-gradient-to-r from-green-400 to-lime-400  hover:bg-blue-700 text-sm text-white py-3 px-10 rounded-full"
              >
                Detail &nbsp;
                
              </button>
            </div> */}
          </Link>

          </div>
        </div>
      </div>
    </section>
  );
};

export default CardCareer;
