'use client';
import { SearchOutlined } from "@ant-design/icons"
import SectionTitle from "../Common/SectionTitle"
import CardCareer from "./Card"
import FormFilter from "./Form"
import { Select } from "antd"
import _ from 'lodash'
import { useEffect, useState } from "react";
const Career = ({ career }) => {
  const [newCareer, setNewCareer] = useState([])
  useEffect(() => {
    setNewCareer(career.data);
  }, [])
  const position = [{
    value:"jakarta", label:"Jakarta",
    value:"surabaya", label:"Surabaya",
    value:"semarang", label:"Semarang",
    value:"bandung", label:"Bandung",
  }]

  const location = [{
    value:"jakarta", label:"Jakarta",
    value:"surabaya", label:"Surabaya",
    value:"semarang", label:"Semarang",
    value:"bandung", label:"Bandung",
  }]
  const [newPosition, setNewPosition] = useState('')
  const handlePositionChange = (newValue) => {
    setNewPosition(newValue)
  };
  const [newLocation, setNewLocation] = useState('')
  const handleLocationChange = (newValue) => {
    setNewLocation(newValue)
  };
  const [searchText, setSearchText] = useState('')
  const searchChange = (e)=> {
    setSearchText(e.target.value)
  }
  const searchClick = () => {
    if (newPosition) {
      setNewCareer(
        career.data.filter(item => item.attributes.position === newPosition)
      )
    }
    if (newLocation) {
      setNewCareer(
        career.data.filter(item => item.attributes.location === newLocation)
      )
    }
    if(searchText){
      setNewCareer(
        career.data.filter(item => item.attributes.location === searchText || item.attributes.position === searchText)
      )
    }

  }

  return (
    <section id="contact" className="overflow-hidden py-2 md:py-4 lg:py-8">
      <div className="">
        <SectionTitle title="Karir" paragraph="" dark={false} />
        <section className="">
          <div className="w-full my-4 p-4 rounded-md border-2 bg-white shadow-xl">
            <div className="flex">
              <div className="w-full px-2 md:w-1/4">
                <label className="block mb-1" >Cari</label>
                <input onChange={searchChange} placeholder="Cari posisi, lokasi" className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline" type="text" id="formGridCode_name" />
              </div>
              <div className="w-full px-2 md:w-1/4">
                <label className="block mb-1" >Posisi</label>
                <Select
                  defaultValue="Finance"
                  style={{ width: '100%', height: "40px" }}
                  options={position.map(item => {
                    return { value: item.position, label: item.position }
                  })}
                  onChange={handlePositionChange}
                />
              </div>
              <div className="w-full px-2 md:w-1/4">
                <label className="block mb-1" >Lokasi</label>
                <Select
                  defaultValue="Jakarta"
                  style={{ width: '100%', height: "40px" }}
                  options={location.map(item => {
                    return { value: item.location, label: item.location }
                  })}
                  onChange={handleLocationChange}
                />
              </div>
              <div className="text-center w-full px-2 md:w-1/4 self-end">
                <button onClick={searchClick} type="submit" className=" font-semibold  bg-dassaGreen  hover:bg-[#405B03] text-lg text-white py-2 px-6 rounded-full">
                  CARI &nbsp;
                  &nbsp;
                  <SearchOutlined style={{ fontSize: 20 }} />
                </button>
              </div>

            </div>
          </div>
        </section>
        <CardCareer key={1} id={1} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={2} id={2} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={3} id={3} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={4} id={4} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={5} id={5} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={6} id={6} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
        <CardCareer key={7} id={7} position={"Staff Finance"} location={"Jakarta"} dueDate={"12-09-2024"}  />
       

      </div>

    </section>
  )
}

export default Career