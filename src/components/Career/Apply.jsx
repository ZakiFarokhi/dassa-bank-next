'use client'
import React, { useState } from 'react';
import { Button, Cascader, DatePicker, Form, Input, InputNumber, Modal, Radio, Select, Switch, TreeSelect, Upload, UploadProps, message } from 'antd';
import { ArrowRightOutlined, UploadOutlined } from '@ant-design/icons';

const FormApply= () => {
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('Content of the modal');

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = () => {
    setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    setOpen(false);
  };
  const props = {
    beforeUpload: (file) => {
      const isPNG = file.type === 'image/png';
      if (!isPNG) {
        message.error(`${file.name} is not a png file`);
      }
      return isPNG || Upload.LIST_IGNORE;
    },
    onChange: (info) => {
      console.log(info.fileList);
    },
  };
  return (
    <>

      <button className="font-semibold text-lg bg-dassaGreen  hover:bg-blue-700 text-sm text-white py-3 px-8 rounded-full" onClick={showModal}>
        Lamar &nbsp;
        <ArrowRightOutlined style={{ fontSize: 20 }} />
      </button>
      <Modal
        title="Form"
        open={open}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        footer={''}
      >
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 14 }}
          layout="vertical"
          initialValues={{ size: 'default' }}
          size="middle"
          style={{ maxWidth: 600 }}
        >
          <Form.Item label="Nama">
            <Input />
          </Form.Item>
          <Form.Item label="Tanggal Lahir">
            <DatePicker />
          </Form.Item>
          <Form.Item label="Jenis Kelamin">
            <Select>
              <Select.Option value="pria">Pria</Select.Option>
              <Select.Option value="wanita">Wanita</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="Telepon">
            <Input />
          </Form.Item>
          <Form.Item label="Alamat">
            <Input />
          </Form.Item>
          <Form.Item label="Resume">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Upload png only</Button>
            </Upload>
          </Form.Item>


          <Form.Item>
            <button className="font-semibold text-lg bg-dassaGreen  hover:bg-blue-700 text-white py-3 px-8 rounded-full" onClick={showModal}>
              Kirim &nbsp;
              <ArrowRightOutlined style={{ fontSize: 20 }} />
            </button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default FormApply;