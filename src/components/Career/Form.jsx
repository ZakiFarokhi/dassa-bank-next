'use client';
import { ArrowRightOutlined, LockOutlined, SearchOutlined, UserOutlined } from "@ant-design/icons"
import { Button, Form, Input, Select } from "antd"
import { useState } from "react";

const FormFilter = () => {
  const [form] = Form.useForm();
  const [clientReady, setClientReady] = useState<boolean>(false);


  const onFinish = (values) => {
    console.log('Finish:', values);
  };

  return (
    <section className="container">
      <div className="w-full mx-8 my-4 p-4 rounded-md border-2 bg-white shadow-xl">
        <div className="flex">
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1" >Cari</label>
            <input placeholder="Cari posisi, lokasi" className="w-full h-10 px-3 text-base placeholder-gray-600 border rounded-lg focus:shadow-outline" type="text" id="formGridCode_name" />
          </div>
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1" >Posisi</label>
            <Select
              defaultValue="Finance Staff"
              style={{ width: '100%', height: "40px" }}
              options={[
                { value: 'jack', label: 'Jack' },
                { value: 'lucy', label: 'Lucy' },
                { value: 'Yiminghe', label: 'yiminghe' },
                { value: 'disabled', label: 'Disabled', disabled: true },
              ]}
            />
          </div>
          <div className="w-full px-2 md:w-1/4">
            <label className="block mb-1" >Lokasi</label>
            <Select
              defaultValue="Jakarta"
              style={{ width: '100%', height: "40px" }}
              options={[
                { value: 'jack', label: 'Jack' },
                { value: 'lucy', label: 'Lucy' },
                { value: 'Yiminghe', label: 'yiminghe' },
                { value: 'disabled', label: 'Disabled', disabled: true },
              ]}
            />
          </div>
          <div className="text-center w-full px-2 md:w-1/4 self-end">
            <button type="submit" className=" font-semibold text-lg bg-dassaGreen  hover:bg-[#405B03] hover:text-white  text-white py-2 px-4 rounded-full">
              Cari &nbsp;
              <SearchOutlined style={{ fontSize: 20 }} />
            </button>
          </div>

        </div>

      </div>
    </section>


  )
}

export default FormFilter